<?php

return array(
    'dsn' => "http://8f948f3bcdbd41cf859daa7f2fb51c3e:3cad1704db5a4cbcab706821aa5b6ff6@sentry.imbco.ir/17",

    // capture release as git sha
    // 'release' => trim(exec('git log --pretty="%h" -n1 HEAD')),

    // Capture bindings on SQL queries
    'breadcrumbs.sql_bindings' => true,

    // Capture default user context
    'user_context' => false,
);
