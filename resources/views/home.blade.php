@extends('layouts.app')

@section('content')
    <div class="banner-banner">
        <h1 class="text-center my-5">This is an API Service</h1>
        <div class="banner home-banner video-banner">
            <video style="width:100%" autoplay="true" loop="" muted="" id="video_background" preload="auto">
                <source src="{{ asset("images/cat.mp4") }}" type="video/mp4">
            </video>
        </div>
        <div class="banner-text color-black"></div>
    </div>
@endsection
