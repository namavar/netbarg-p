<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'کلمات عبور باید حداقل شش کاراکتر باشد و با تأیید مطابقت دارند.',
    'reset' => 'پسورد جدید بازنشانی گردید',
    'sent' => 'پسورد جدید برای شما ایمیل شد.',
    'token' => 'این رمز عبور بازنشانی رمز ورود نامعتبر است',
    'user' => "کاربری با این آدرس ایمیل یافت نشد.",

];

