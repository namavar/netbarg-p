<?php
/*
|--------------------------------------------------------------------------
| All Response Message
|--------------------------------------------------------------------------
|
| All messages for send response for all requests
|
*/

return [

    /* public Transe Response Part Message  */
    'success_insert_data' => 'اطلاعات با موفقیت ثبت شد.',
    'access_forbiden' => 'شما دسترسی به این قسمت را ندارید.',
    'success_delete_data' => 'اطلاعات با موفقیت حذف شد.',
    'error_insert_data' => 'خطا در ثبت اطلاعات',
    'role_not_invalid' => 'سطح دسترسی مشخص شده اشتباه است.',
    'success_update_data' => 'اطلاعات با موفقیت به روز رسانی شد.',
    'error_username_password' => 'نام کاربری یا کلمه عبور اشتباه است',
    'not_found_phone_number' => 'شماره مورد نظر یافت نشد',
    'logout_success' => 'شما با موفقیت از حساب کاربری خود خارج شدید.',
    'error_update_data' => 'به روزرسانی اطلاعات با خطا مواجه شد.',
    'success_delete_data' => 'اطلاعات با موفقیت حذف شد.',
    'is_not_owner_company' => 'کمپانی انتخاب شده متعلق به شما نمی باشد.',





    /* OtpService Trans Part Message */
    'otpService' => [
        'invalid_phone_number' => 'شماره تلفن همراه صحیح نمی‌باشد',
        'repeat_phone_number' => 'شماره تلفن همراه قبلا ثبت گردیده است',
        'access_denie_temperory' => 'دسترسی شما به این سرویس موقتا امکان پذیر نمی‌باش',
        'error_exist_retry' => 'بروز خطا. لطفا مجددا تلاش کنید',
        'send_four_number' => 'کد ۴ رقمی به تلفن شما ارسال شد.',
        'please_check_not_robot' => 'لطفا تیک من ربات نیستم را بزنید',
        'retry_please_check_not_robot' => 'لطفا مجددا تیک من ربات نیستم را بزنید',
        'mistak_four_number' => 'کد ۴ رقمی وارد شده اشتباه است',
        'error_exist_send_code_retry' => 'بروز خطا در هنگام بررسی رمز یکبار مصرف. لطفا مجددا تلاش کنید',
        'expired_code_resend_new_code' => 'رمز یکبار مصرف منقضی گردیده است. رمز جدید مجددا ارسال شد'
    ],


    /* platformMenuService Trans Part Message */
    'platformMenuService' => [
        'success_insert' => 'ثبت با موفقیت انجام شد',
        'error_insert_data' => 'خطا در ثبت اطلاعات',
        'success_update_data' => 'بروز رسانی با موفقیت انجام شد.',
        'error_update_data' => 'مشکلی در بروز رسانی به وجود آمده است.',
    ],


    /* otpMessageFormatter Transe Part Message */
    'otpMessageFormatter' => [
        'code_auth_is' => 'کد احراز هویت شما %s می باشد'
    ],

    /* attachments Trans Part Message */
    'attachments' => [
        'error_upload_data' => 'خطا در آپلود فایل'
    ]







];
