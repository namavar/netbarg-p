<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlatformMenuIndexRequest;
use App\Http\Requests\PlatformMenusCreateRequest;
use App\Http\Requests\PlatformMenusUpdateRequest;
use App\Repositories\PlatformMenuRepository;
use App\Services\PlatformMenuService;
use Illuminate\Support\Facades\Auth;

/**
 * Class PlatformMenusController.
 *
 * @package namespace App\Http\Controllers;
 */
class PlatformMenusController extends Controller
{
    /**
     * @var PlatformMenuRepository
     */
    protected $repository;
    protected $platformMenuService;

    /**
     * PlatformMenusController constructor.
     *
     * @param PlatformMenuRepository $repository
     * @param PlatformMenuService $platformMenuService
    */

    public function __construct(PlatformMenuRepository $repository,PlatformMenuService $platformMenuService)
    {
        $this->repository = $repository;
        $this->platformMenuService = $platformMenuService;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     * @var PlatformMenuIndexRequest $request
     */
    public function index(PlatformMenuIndexRequest $request)
    {
        $filters = $request->request->get('filters');
        $data = $this->platformMenuService->getMenus($filters);
        return response()->json($data);
    }

    /**
     * Add a newly created resource in storage.
     *
     * @param  PlatformMenusCreateRequest $request
     *
     * @return \Illuminate\Http\Response
    */

    public function store(PlatformMenusCreateRequest $request)
    {
        $response = $this->platformMenuService->createPlatformMenu($request->all());
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PlatformMenusUpdateRequest $request
     * @param  string $id
     *
     * @return \Illuminate\Http\Response
    */
    public function update(PlatformMenusUpdateRequest $request, $id)
    {
        $response = $this->platformMenuService->updatePlatformMenu($request->all(), $id);
        return response()->json($response);
    }

    /**
     * get menus with user access.
     *
     * @return \Illuminate\Http\Response
    */

    public function getSideBarMenus()
    {
        $user = Auth::guard('api')->user();
        $response = $this->platformMenuService->getMenusWithUserRole($user->role_id);
        return response()->json($response);
    }

    /**
     * get all parent menus.
     *
     * @return \Illuminate\Http\Response
    */

    public function getParents(){
        $response = $this->platformMenuService->getParents();
        return response()->json($response);
    }
}
