<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleEditRequest;
use App\Http\Requests\RoleListRequest;
use App\Http\Requests\RolePermissionRequest;
use App\Services\RoleService;
use App\Http\Requests\RoleCreateRequest;
use App\Repositories\RoleRepository;

/**
 * Class RolesController.
 *
 * @package namespace App\Http\Controllers;
 */
class RolesController extends Controller
{
    /**
     * @var RoleRepository
     */
    protected $repository;

    /**
     * RolesController constructor.
     *
     * @param RoleRepository $repository
     */
    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(RoleListRequest $request,RoleService $roleService)
    {
        $response = $roleService->listRoles($request);
        return $response;
    }

    public function store(RoleCreateRequest $request,RoleService $roleService)
    {
        $response = $roleService->createRole($request->all());
        return $response;
    }

    public function show($id,RoleService $roleService)
    {
        $response = $roleService->findRole($id);
        return $response;
    }

    public function update(RoleEditRequest $request,$id,RoleService $roleService)
    {
        $response = $roleService->editRole($request->all(),$id);
        return $response;
    }

    public function destroy($id,RoleService $roleService)
    {
        $response = $roleService->destroyRole($id);
        return $response;
    }

    public function addRolePermission(RolePermissionRequest $request,RoleService $roleService)
    {
        $response = $roleService->addRolePermission($request->all());
        return $response;
    }

    public function removeRolePermission(RolePermissionRequest $request, RoleService $roleService)
    {
        $response = $roleService->removeRolePermission($request->all());
        return $response;
    }

    public function getPermisions(RoleService $roleService)
    {
        $response = $roleService->getPermisions();
        return response()->json($response);
    }   
}
