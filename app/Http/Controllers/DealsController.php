<?php

namespace App\Http\Controllers;

use App\Http\Requests\DealIndexRequest;
use App\Http\Requests\Deals\DealCreateRequest;
use App\Repositories\DealRepository;
use App\Services\DealsService;
use Illuminate\Support\Facades\Auth;

class DealsController extends Controller
{
    protected  $dealRepository;
    protected  $dealsService;

    public function __construct(DealRepository $dealRepository, DealsService $dealsService)
    {
        $this->dealRepository = $dealRepository;
        $this->dealsService = $dealsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param DealIndexRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(DealIndexRequest $request)
    {
        $user = Auth::guard('api')->user();
        $filters = $request->request->get('filters');

        $data = $this->dealsService->getDealsByFilter($user, $filters);
        return response()->json($data);    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DealCreateRequest $request)
    {
        $user = auth()->user();

        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DealCreateRequest $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
