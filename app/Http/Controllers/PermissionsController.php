<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShowPermissionRequest;
use App\Repositories\PermissionRepository;

/**
 * Class PermissionsController.
 *
 * @package namespace App\Http\Controllers;
 */
class PermissionsController extends Controller
{
    /**
     * @var PermissionRepository
     */
    protected $repository;

    /**
     * PermissionsController constructor.
     *
     * @param PermissionRepository $repository
     */
    public function __construct(PermissionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $response = $this->repository->getAllPrefix();
        $count = $response->count();
        return response()->json([
            'status' => true,
            'message' => '',
            'data' => [
                'count' => $count,
                'permissions' => $response,
            ]
        ], 200);
    }

    public function show(ShowPermissionRequest $request)
    {
        $data = $this->repository->getByPrefix($request->all());
        return response()->json([
            'status' => true,
            'message' => "",
            'data' => $data,
        ]);
    }
}
