<?php

namespace App\Http\Controllers;

use App\Constants\ContractConstants;
use App\Http\Requests\AttachmentsRequest;
use App\Http\Requests\AttachmentsUpdateRequest;
use App\Http\Requests\Contracts\ContractCategoryConstRequest;
use App\Http\Requests\Contracts\ContractCreateRequest;
use App\Http\Requests\Contracts\ContractListRequest;
use App\Http\Requests\Contracts\ContractRemoveRequest;
use App\Http\Requests\Contracts\ContractUpdateRequest;
use App\Http\Requests\Contracts\ContractShowRequest;
use App\Repositories\ContractsRepository;
use App\Serializers\Contracts\DealIndexCollection;
use App\Services\AttachmentsService;
use App\Services\ContractsService;
use Illuminate\Support\Facades\Auth;



class ContractsController extends Controller
{


    /**
     * @var ContractsRepository
     */
    protected $repository;
    protected $contractsService;
    protected $attachmentsService;

    /**
     * CompaniesController constructor.
     *
     * @param ContractsRepository $repository
     * @param ContractsService $contractsService
     * @param AttachmentsService $attachmentsService
     */
    public function __construct(ContractsRepository $repository, ContractsService $contractsService, AttachmentsService $attachmentsService)
    {
        $this->repository = $repository;
        $this->contractsService = $contractsService;
        $this->attachmentsService = $attachmentsService;
    }


    /**
     * Display a listing of the resource.
     * @var ContractListRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(ContractListRequest $request)
    {
;
        /** @var ContractsService $contractsService */
        $data = $this->contractsService->getContract($request->all());
        return response()->json($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     *
     * @var ContractCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContractCreateRequest $request)
    {
        $user = Auth::user();
        $response = $this->contractsService->createContract($request->all(), $user);
        return response()->json($response);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $contract_id
     * @param ContractShowRequest $request
     * @return \Illuminate\Http\Response
     */
    public function show(ContractShowRequest $request, $contract_id)
    {
        $user = Auth::user();
        $response = $this->contractsService->ShowContract($request->all(), $contract_id, $user);
        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int $contract_id
     * @param ContractUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(ContractUpdateRequest $request, $contract_id)
    {
        $user = Auth::user();
        $response = $this->contractsService->updateContract($request->all(), $contract_id , $user);
        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     * @param AttachmentsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function uploadFile(AttachmentsRequest $request)
    {
        $response = $this->attachmentsService->uploads($request->all());
        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     * @param AttachmentsUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateUploadFile(AttachmentsUpdateRequest $request, $id)
    {
        $response = $this->attachmentsService->updateUploads($request->all(), $id);
        return response()->json($response);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $contract_id
     * @param  ContractRemoveRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContractRemoveRequest $request, $contract_id)
    {
        $user = Auth::user();
        $response = $this->contractsService->removeContract($request->all() ,$contract_id,  $user);
        return response()->json($response);
    }


    /**
     * Get list Categories from consts
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function constantsCategories()
    {

        $categories = $this->contractsService->constantsCategories();
        return response()->json($categories);
    }


    /**
     * Get data Required from Category with name
     *
     * @param ContractCategoryConstRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRequiredCategory(ContractCategoryConstRequest $request)
    {
        $detailCategories = $this->contractsService->getRequiredCategory($request->id);
        return response()->json($detailCategories);

    }

    /**
     * Get data Required from Category with name
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRole()
    {
        $role = $this->contractsService->getRole();
        return response()->json($role);

    }

}
