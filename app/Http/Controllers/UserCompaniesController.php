<?php

namespace App\Http\Controllers;

use App\Constants\RoleConstants;
use App\Http\Requests\CompanyUser\CompanyUserCreateRequest;
use App\Http\Requests\CompanyUser\CompanyUserRemoveRequest;
use App\Http\Requests\CompanyUser\CompanyUserShowRequest;
use App\Http\Requests\CompanyUser\CompanyUserUpdateRequest;
use App\Http\Requests\CompanyUser\CompanyUserListRequest;
use App\Services\CompanyUserService;
use App\Repositories\CompanyUserRepository;
use Illuminate\Support\Facades\Auth;


class UserCompaniesController extends Controller
{

    /**
     * @var CompanyUserRepository
     */
    protected $repository;
    protected $companyUserService;

    /**
     * CompaniesController constructor.
     *
     * @param CompanyUserRepository $repository
     * @param CompanyUserService $companyUserService
     */
    public function __construct(CompanyUserRepository $repository, CompanyUserService $companyUserService)
    {
        $this->repository = $repository;
        $this->companyUserService = $companyUserService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @var CompanyUserListRequest $request
     */
    public function index(CompanyUserListRequest $request)
    {
        $filters = $request->request->get('filters');
        $user = Auth::user();

        /** @var CompanyUserService $companyUserService */
        $data = $this->companyUserService->getCompanyUserByFilter($request->all(), $user, $filters);
        return response()->json($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CompanyUserCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyUserCreateRequest $request)
    {
        $user = Auth::user()->id;
        $response = $this->companyUserService->createCompanyUser($request->all(), $user);
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $userId
     * @param CompanyUserUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyUserUpdateRequest $request, $userId)
    {
        $user_login = Auth::user();
        $response = $this->companyUserService->updateCompanyUser($request->all(), $userId, $user_login);
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $userId
     * @param CompanyUserShowRequest $request
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyUserShowRequest $request, $userId)
    {
        $roleId = Auth::user()->role_id;
        $response = $this->companyUserService->ShowCompanyUser($request->all(), $userId, $roleId);
        return response()->json($response);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $userId
     * @param CompanyUserRemoveRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyUserRemoveRequest $request, $userId)
    {
        $roleId = Auth::user()->role_id;
        $response = $this->companyUserService->removeCompanyUser($request->all(), $userId, $roleId);
        return response()->json($response);
    }
}
