<?php

namespace App\Http\Controllers;

use App\Constants\RoleConstants;
use App\Http\Requests\CompanyCreateRequest;
use App\Http\Requests\CompanyIndexRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Http\Requests\CompanyZoneRequest;
use App\Http\Requests\userCompaniesRequest;
use App\Repositories\CityRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\ZoneRepository;
use App\Services\CompanyService;
use Illuminate\Support\Facades\Auth;

/**
 * Class CompaniesController.
 *
 * @package namespace App\Http\Controllers;
 */
class CompaniesController extends Controller
{
    /**
     * @var CompanyRepository
     */
    protected $repository;
    protected $cityRepository;
    protected $zoneRepository;
    protected $companyService;

    /**
     * CompaniesController constructor.
     *
     * @param CompanyRepository $repository
     * @param CityRepository $cityRepository
     * @param ZoneRepository $zoneRepository
     * @param CompanyService $companyService
     */
    public function __construct(CompanyRepository $repository, CityRepository $cityRepository,
                                ZoneRepository $zoneRepository, CompanyService $companyService)
    {
        $this->repository = $repository;
        $this->cityRepository = $cityRepository;
        $this->zoneRepository = $zoneRepository;
        $this->companyService = $companyService;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     * @var CompanyIndexRequest $request
     */
    public function index(CompanyIndexRequest $request)
    {
        $user = Auth::guard('api')->user();
        $isAdmin = false;
        if ($user && $user->role_id == RoleConstants::ROLE_ADMIN_ID) {
            $isAdmin = true;
        }
        $filters = $request->request->get('filters');

        $data = $this->companyService->getCompaniesByFilter($user, $filters, $isAdmin);
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CompanyCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyCreateRequest $request)
    {
        $user = Auth::user();
        $response = $this->companyService->createCompany($request->all(),$user);
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $response = $this->companyService->getCompany($id,$user);
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CompanyUpdateRequest $request
     * @param  string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyUpdateRequest $request, $id)
    {
        $user = Auth::user();
        $response = $this->companyService->updateCompany($request->all(), $id, $user);
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $response = $this->companyService->removeCompany($id,$user);
        return response()->json($response);
    }

    public function getCities()
    {
        $response = $this->cityRepository->getAllCities();
        return response()->json([
            'status' => true,
            'message' => '',
            'data' => $response,
        ]);
    }

    public function getZones(CompanyZoneRequest $request)
    {
        $response = $this->zoneRepository->getZones($request->all());
        return response()->json([
            'status' => true,
            'message' => '',
            'data' => $response,
        ]);
    }

    public function userCompanies(userCompaniesRequest $request)
    {
        $user = Auth::guard('api')->user();
        $isAdmin = false;
        if ($user && $user->role_id == RoleConstants::ROLE_ADMIN_ID) {
            $isAdmin = true;
        }

        $data = $this->companyService->getUserCompanies($user, $isAdmin);
        return response()->json($data);

    }
}
