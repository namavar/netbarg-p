<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserOtpRequest;
use App\Http\Requests\VerifyPhoneOnLoginRequest;
use App\Services\UserService;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UserLoginRequest;
use Illuminate\Http\Request;

/**
 * Class UsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class UsersController extends Controller
{
    public function login(UserLoginRequest $request, UserService $userService)
    {
        $response = $userService->login($request->all());
        return response()->json($response);
    }

    public function otpLogin(UserOtpRequest $request, UserService $userService)
    {
        $response = $userService->otpLogin($request->all());
        return response()->json($response);
    }

    public function register(UserRegisterRequest $request, UserService $userService)
    {
        $response = $userService->createNewUser($request->all());
        return response()->json($response);
    }

    public function logout(Request $request, UserService $userService)
    {
        $response = $userService->logout($request);
        return response()->json($response);
    }

    public function verifyPhoneOnLogin(VerifyPhoneOnLoginRequest $request,UserService $userService)
    {
        $response = $userService->verifyPhoneOnLogin($request);
        return response()->json($response);
    }
}
