<?php

namespace App\Http\Middleware;

use App\Constants\RoleConstants;
use Closure;
use Auth;
use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\App;
use Illuminate\Foundation\Application;

class Acl
{
    private  $app;
    public function __construct(Application $app)
    {
        $this->app = $app;

    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->Route();
        $name = $route->getPrefix() . "/" . $route->getActionMethod();
        $prefix = $route->getPrefix();
        if (Auth::guest()) {
            abort(403, Lang::get('auth.limmit_access'));
        }
        
        $roleId = Auth::user()->role_id;
        
        if ($roleId == RoleConstants::ROLE_ADMIN_ID) {
            return $next($request);
        }

        $query = DB::table('role_has_permissions')
            ->join('permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
            ->where([
                ['role_has_permissions.role_id', $roleId],
                ['permissions.name', $name],
                ['permissions.prefix', $prefix]
            ])->count();

        if ($query == 0 && in_array($this->app->environment(), ['prod', 'production'])) {
            abort(403, Lang::get('auth.limmit_access'));
        }

        return $next($request);
    }
}
