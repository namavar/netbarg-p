<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlatformMenusCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'alias' => 'required',
            'parent_id' => 'sometimes|integer',
            'url' => 'sometimes|string',
            'icon' => 'sometimes|string',
            'permission_id' => 'sometimes|integer',
        ];
    }
}
