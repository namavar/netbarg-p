<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShowPermissionRequest extends FormRequest
{
    public function rules()
    {
        return [
            'prefix' => 'required'
        ];
    }
}
