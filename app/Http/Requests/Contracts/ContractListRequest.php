<?php

namespace App\Http\Requests\Contracts;

use Illuminate\Foundation\Http\FormRequest;

class ContractListRequest extends FormRequest
{
    public function rules()
    {
        return [
            'company_id' => 'required|integer'
        ];
    }
}
