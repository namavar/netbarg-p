<?php

namespace App\Http\Requests\Contracts;

use Illuminate\Foundation\Http\FormRequest;

class ContractCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'required|integer',
            'company_name' => 'required|string',
            'name' => 'required|string',
            'email' => 'required|string|email|max:255',
            'sheba_number' => 'required|string',
            'contract_owner_name' => 'required|string',
            'national_code' => 'required|string',
            'bank_name' => 'required|string',
            'category_id' => 'required|integer',
            'card_number' => 'required|string'

        ];
    }
}
