<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class VerifyPhoneOnLoginRequest extends FormRequest
{
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:255',
            'phone' => 'required|regex:/(09)[0-9]{9}/',
            'confirm_code' => 'sometimes|required|digits:4'
        ];
    }
}
