<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserOtpRequest extends FormRequest
{
    public function rules()
    {
        return [
            'phone' => 'required|regex:/(09)[0-9]{9}/',
        ];
    }
}
