<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => 'unique:companies',
            'website' => 'sometimes|url',
            'phone' => 'required|numeric',
            'address' => 'required',
            'zip_code' => 'required|integer',
            'city_id' => 'required|integer',
            'zone' => 'required|string',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'map_zoom_level' => 'required|integer',
            'is_discount' => 'sometimes',
            'has_sellercenter' => 'sometimes'
        ];
    }
}
