<?php

namespace App\Http\Requests\CompanyUser;

use Illuminate\Foundation\Http\FormRequest;

class CompanyUserUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'sometimes|string|min:6|confirmed',
            'role_id' => 'required|integer',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'gender_id' => 'sometimes|integer',
            'city_id' => 'sometimes|integer',
            'state_id' => 'sometimes|string',
            'national_code' => 'sometimes|string',
            'phone' => 'sometimes|regex:/(09)[0-9]{9}/',
            'active'  => 'sometimes|integer',
            'companies' => 'sometimes',
        ];
    }
}
