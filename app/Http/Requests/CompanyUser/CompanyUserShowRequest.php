<?php

namespace App\Http\Requests\CompanyUser;

use Illuminate\Foundation\Http\FormRequest;

class CompanyUserShowRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'required|integer'
        ];
    }
}
