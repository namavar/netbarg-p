<?php
/**
 * Created by PhpStorm.
 * User: bahador
 * Date: 1/7/19
 * Time: 11:51 AM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleEditRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'guard_name' => 'required'
        ];
    }
}
