<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 2/25/19
 * Time: 12:24 PM
 */

namespace App\Http\Requests\Deals;


use Illuminate\Foundation\Http\FormRequest;

class DealCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contract_id' => 'required|integer',
            'name' => 'required|string',
            'short_name' => 'required|string',
            'private_note' => 'string',
            'desciption' => 'required|string',
            'conditions' => 'required|string',
            'highlights' => 'required|string',
            'original_price' => 'required|intiger',
            'discount_amount' => 'required|intiger',
            'discount_percentage' => 'required|intiger',
            'start_date' => 'required|string',
            'end_date' => 'required|string',
            'coupon_start_date' => 'required|string',
            'coupon_end_date' => 'required|string',
            'tax' => 'required|intiger',
            'min_limit' => 'required|intiger',
            'max_limit' => 'required|intiger',
            'min_limit_per_user' => 'required|intiger',
            'max_limit_per_user' => 'required|intiger',
            'gender_id' => 'required|intiger',
            'brand_id' => 'intiger',
            'for_one_user' => 'intiger',
            'hard_print_needed' => 'intiger',
            'reserve_needed' => 'intiger',
            'is_especial' => 'intiger',
            'is_especial' => 'intiger',
            'is_main' => 'intiger',
            'is_postal' => 'intiger',
            'category_id' => 'intiger',
            'for_all_cities' => 'intiger',
        ];
    }
}
