<?php

namespace App\vendor\Otp;


use Illuminate\Support\Facades\Lang;

class OtpMessageFormatter implements OtpFormatterInterface
{
    /**
     * @param $code
     * @return string
     */
    public function format($code)
    {
        return sprintf(Lang::get('response.otpMessageFormatter.code_auth_is'), $code);
    }
}
