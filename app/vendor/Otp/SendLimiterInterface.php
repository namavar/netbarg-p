<?php

namespace App\vendor\Otp;


interface SendLimiterInterface
{
    /**
     * This method must return a integer value that presents time range in seconds
     *
     * e.g: 3600 for one hour.
     *
     * @return int
     */
    public function getTimeLimitInSeconds();

    /**
     * Get maximum amount of items that we can send in the time limit
     * e.g: for 10 messages per hour, return 10.
     *
     * @return int
     */
    public function getMax();
}
