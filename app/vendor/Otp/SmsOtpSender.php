<?php

namespace App\vendor\Otp;

class SmsOtpSender implements OtpSenderInterface
{
    private $api;
    private $userName;
    private $password;

    public function __construct($settings)
    {
        $this->api = $settings['api'];
        $this->userName = $settings['username'];
        $this->password = $settings['password'];
    }
    /**
     * @param $phoneNumber
     * @param OtpStorageInterface $store
     * @param SendLimiterInterface $limiter
     * @return bool
     * @throws OtpException
     */
    public function checkSendLimit($phoneNumber, OtpStorageInterface $store, SendLimiterInterface $limiter)
    {
        $sent = $store->getNumberOfSentItemsInLimit($phoneNumber);

        if ($sent + 1 > $limiter->getMax()) {
            throw new OtpException("send limit exceeded. max: " . $limiter->getMax() . ' items in ' .
                $limiter->getTimeLimitInSeconds() . ' seconds', OtpException::SEND_LIMIT_EXCEEDED);
        }

        return true;
    }

    /**
     * Send SMS to user using SOAP
     *
     * @param string $to
     * @param string $message
     * @throws \Exception
     */
    private function sendSmsToUser($to, $message)
    {

        if (!isset($this->userName) || !isset($this->password) || !isset($this->api)) {
            throw new \Exception("settings are not correct. username, password and api must be stored in the settings table");
        }

        /** @var \SoapClient $soapClient */
        $soapClient = new \SoapClient($this->api, [
            'encoding' => 'UTF-8',
            'connection_timeout' => 15,
            'use' => SOAP_LITERAL,
            'style' => SOAP_DOCUMENT
        ]);

        // I just copy and pasted this code from `AdpComponent`
        $smsSendParams = new \stdClass();
        $smsSendParams->userName = $this->userName;
        $smsSendParams->password = $this->password;
        $smsSendParams->sourceNo = env('OTP_ORIGINATOR_NUMBER', '98200042091100');
        // replace leading 0 with 98
        // eg: 09123456789 to 989123456789
        $smsSendParams->destNo = preg_replace('#^0(\d{10})$#', '98$1', $to);
        $smsSendParams->sourcePort = null;
        $smsSendParams->destPort = null;
        $smsSendParams->clientId = array();
        $smsSendParams->messageType = 1;
        $smsSendParams->encoding = 2;
        $smsSendParams->longSupported = true;
        $smsSendParams->dueTime = time();
        $smsSendParams->content = $message;

        $wsResponse = $soapClient->send($smsSendParams);

        if (intval($wsResponse->sendReturn->status) !== 0) {
            // error happened. throw exception
            throw new \Exception("SMS send error. webservice returned status: " . $wsResponse->sendReturn->status);
        }
    }

    /**
     * Send Otp to user.
     *
     * @param $userMobileNumber
     * @param PasswordGeneratorInterface $generator
     * @param OtpFormatterInterface $formatter
     * @param OtpStorageInterface $otpStore
     * @return mixed|void
     * @throws OtpException
     */
    public function send(
        $userMobileNumber,
        PasswordGeneratorInterface $generator,
        OtpFormatterInterface $formatter,
        OtpStorageInterface $otpStore
    ) {
        // generate code
        $code = $generator->generate();
        // put code inside the message (format message)
        $message = $formatter->format($code);

        try {
            // send message to user
            $this->sendSmsToUser($userMobileNumber, $message);
            // calculate expiration time of the message
            $expireTime = time() + (60 * 5);

            $item = new OtpStorageItem($userMobileNumber, $code, date('Y-m-d H:i:s'), date('Y-m-d H:i:s', $expireTime));

            // store code using the given store driver
            $otpStore->store($item);

        } catch (\Exception $e) {

            //TODO: log or report exception for monitoring the OTP service

            throw new OtpException($e->getMessage(), OtpException::SEND_FAILURE);
        }
    }

}

