<?php
/**
 * Created by PhpStorm.
 * User: bahador
 * Date: 12/25/18
 * Time: 4:18 PM
 */

namespace App\vendor\Otp;


class OneHourSendLimit implements SendLimiterInterface
{
    /**
     * @inheritdoc
     * @return int
     */
    public function getTimeLimitInSeconds()
    {
        return 3600;
    }

    /**
     * Get maximum amount of items that we can send in the time limit
     * @inheritdoc
     * @return int
     */
    public function getMax()
    {
        return 3;
    }

}
