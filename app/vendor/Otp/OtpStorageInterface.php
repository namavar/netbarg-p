<?php

namespace App\vendor\Otp;


interface OtpStorageInterface
{
    /**
     * @param OtpStorageItem $otpStorageItem
     * @return mixed
     */
    public function store(OtpStorageItem $otpStorageItem);

    /**
     * @param $mobile
     * @return null|OtpStorageItem
     */
    public function getPassword($mobile);

    /**
     * @param $mobile
     * @return int
     */
    public function getNumberOfSentItemsInLimit($mobile);

    /**
     * @param $mobile
     * @param SendLimiterInterface $limiter
     * @return mixed
     */
    public function updateSentCount($mobile, SendLimiterInterface $limiter);

    /**
     * @param $mobile
     * @return mixed
     */
    public function resetSendLimit($mobile);

    /**
     * This method should return how much user should wait to be able receive otp again.
     *
     * @param $mobile
     * @return mixed
     */
    public function getLimitRemainingTimeInSeconds($mobile);

    /**
     * @param $mobile
     * @return int
     */
    public function increaseFailedAttemptsCount($mobile);

    /**
     * @param $mobile
     * @return int
     */
    public function getFailedAttemptsCount($mobile);
}

