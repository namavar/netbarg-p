<?php

namespace App\vendor\Otp;


class OtpVerifier
{
    public function verify($userEnteredPassword, $userMobileNumber, OtpStorageInterface $otpStore)
    {
        $storageItem = $otpStore->getPassword($userMobileNumber);

        if (!$storageItem) {
            return false;
        }

        if ($storageItem->isExpired()) {
            $otpStore->increaseFailedAttemptsCount($userMobileNumber);
            throw new OtpException("OTP has expired. please try again.", OtpException::OTP_EXPIRED);
        }

        $isCorrect = strval($storageItem->password) === strval($userEnteredPassword);
        if (!$isCorrect) {
            $otpStore->increaseFailedAttemptsCount($userMobileNumber);
        }

        return $isCorrect;
    }
}
