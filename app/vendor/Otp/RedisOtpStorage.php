<?php

namespace App\vendor\Otp;


use Illuminate\Support\Facades\Redis;

class RedisOtpStorage implements OtpStorageInterface
{
    /**
     * @var string
     */
    private $redisPrefix = 'OTP_PLUGIN';

    /**
     * @var null|Client
     */
    protected $client = null;

    /**
     * RedisOtpStore constructor.
     */

    function __construct()
    {
        $this->client = Redis::connection();
    }

    /**
     * @return Client
     */
    private function getRedisClient()
    {
        return $this->client;
    }

    /**
     * @param $mobile
     * @return string
     */
    private function getRedisLimitKey($mobile)
    {
        return 'OTP_' . $mobile . '_SEND_COUNT';
    }

    /**
     * @param $mobile
     * @return string
     */
    public function getRedisDataKey($mobile)
    {
        return $this->redisPrefix . '_' . $mobile . '_DATA';
    }

    /**
     * @param $mobile
     * @return string
     */
    public function getRedisFailedTriesKey($mobile)
    {
        return $this->redisPrefix . '_' . $mobile . '_FAILED_TRIES';
    }

    /**
     * Get expiration time in seconds.
     *
     * @return int
     */
    public function getDataExpirationTime()
    {
        return 10 * 60; // (10 minutes)
    }

    /**
     * Update number of sent items
     * @param $mobile
     * @param SendLimiterInterface $limiter
     * @return mixed|void
     */
    public function updateSentCount($mobile, SendLimiterInterface $limiter)
    {
        $client = $this->getRedisClient();
        $value = $this->getNumberOfSentItemsInLimit($mobile);
        $client->set($this->getRedisLimitKey($mobile), $value + 1);
        $client->expire($this->getRedisLimitKey($mobile), $limiter->getTimeLimitInSeconds());
    }

    /**
     * @param $mobile
     * @return mixed|void
     */
    public function resetSendLimit($mobile)
    {
        $this->getRedisClient()->del([$this->getRedisLimitKey($mobile)]);
    }

    /**
     * @param $mobile
     * @return int
     */
    public function getNumberOfSentItemsInLimit($mobile)
    {
        $client = $this->getRedisClient();

        $value = $client->get($this->getRedisLimitKey($mobile));

        return (!empty($value)) ? intval($value) : 0;
    }

    /**
     * @param OtpStorageItem $otpStorageItem
     * @return int|mixed
     */
    public function store(OtpStorageItem $otpStorageItem)
    {
        $client = $this->getRedisClient();
        $setResponse = $client->set($this->getRedisDataKey($otpStorageItem->mobile), (string)$otpStorageItem);
        $client->expire($this->getRedisDataKey($otpStorageItem->mobile), $this->getDataExpirationTime());

        return $setResponse;
    }

    /**
     * @param $mobile
     * @return null|OtpStorageItem
     */
    public function getPassword($mobile)
    {
        $client = $this->getRedisClient();
        $data = $client->get($this->getRedisDataKey($mobile));
        return (is_null($data)) ? null : OtpStorageItem::createFromStdObject(json_decode($data));
    }

    /**
     * Removes the given key from hash
     * @param $mobile
     * @return bool
     */
    public function remove($mobile)
    {
        $client = $this->getRedisClient();
        $client->del([$this->getRedisDataKey($mobile),$this->getRedisFailedTriesKey($mobile)]);

        return true;
    }

    /**
     * @param $mobile
     * @return int|null
     */
    public function getLimitRemainingTimeInSeconds($mobile)
    {
        $client = $this->getRedisClient();
        $s = $client->ttl($this->getRedisLimitKey($mobile));

        return $s;
    }

    /**
     * @param $mobile
     * @return int
     */
    public function increaseFailedAttemptsCount($mobile)
    {
        $client = $this->getRedisClient();
        $tries = $client->get($this->getRedisFailedTriesKey($mobile));
        if (is_null($tries)) {
            $tries = 0;
        }
        $increase = intval($tries) + 1;
        $client->set($this->getRedisFailedTriesKey($mobile), $increase);
        $client->expire($this->getRedisFailedTriesKey($mobile), $this->getDataExpirationTime());

        return $increase;
    }

    /**
     * @param $mobile
     * @return int
     */
    public function getFailedAttemptsCount($mobile)
    {
        $client = $this->getRedisClient();
        $tries = $client->get($this->getRedisFailedTriesKey($mobile));
        if (is_null($tries)) {
            $tries = 0;
        }
        return intval($tries);
    }


}
