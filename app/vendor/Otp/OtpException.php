<?php

namespace App\vendor\Otp;


class OtpException extends \Exception
{
    /**
     * List of predefined error codes
     */
    const FAILURE = 0;
    const SEND_FAILURE = 1;
    const SEND_LIMIT_EXCEEDED = 2;
    const OTP_EXPIRED = 3;

    /**
     * OtpException constructor.
     * @param string $message
     * @param int $code
     */
    function __construct($message = '', $code = 0)
    {
        parent::__construct($message, $code);
    }
}
