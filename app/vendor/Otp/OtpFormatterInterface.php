<?php

namespace App\vendor\Otp;


interface OtpFormatterInterface
{
    /**
     * This method should get the code and return string of message.
     * @param $code
     * @return string
     */
    public function format($code);
}
