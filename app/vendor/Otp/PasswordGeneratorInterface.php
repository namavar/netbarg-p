<?php

namespace App\vendor\Otp;


interface PasswordGeneratorInterface
{
    /**
     * Main method for generating random passwords.
     * Should return a string.
     * @return string
     */
    public function generate();
}
