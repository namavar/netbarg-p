<?php

namespace App\vendor\Otp;


interface OtpSenderInterface
{
    /**
     * This method should check send limits and
     * throw exception if user has exceeded them.
     *
     * @param $phoneNumber
     * @param OtpStorageInterface $store
     * @param SendLimiterInterface $limiter
     * @throws OtpException
     * @return mixed
     */
    public function checkSendLimit($phoneNumber, OtpStorageInterface $store, SendLimiterInterface $limiter);

    /**
     * Main method for sending OTP.
     *
     * @param $userMobileNumber
     * @param PasswordGeneratorInterface $generator
     * @param OtpFormatterInterface $formatter
     * @param OtpStorageInterface $otpStore
     * @return mixed
     */
    public function send(
        $userMobileNumber,
        PasswordGeneratorInterface $generator,
        OtpFormatterInterface $formatter,
        OtpStorageInterface $otpStore
    );
}
