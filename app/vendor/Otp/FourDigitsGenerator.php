<?php

namespace App\vendor\Otp;

class FourDigitsGenerator implements PasswordGeneratorInterface
{
    /**
     * @var null
     */
    protected $password = null;

    /**
     * SimpleNumericPasswordGenerator constructor.
     * Pass password null to generate random passwords
     * @param $password
     */
    function __construct($password = null)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function generate()
    {
        if ($this->password !== null) {
            return $this->password;
        }

        return (string)rand(1111, 9999);
    }
}
