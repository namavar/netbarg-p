<?php
/**
 * Created by PhpStorm.
 * User: bahador
 * Date: 12/25/18
 * Time: 4:13 PM
 */

namespace App\vendor\Otp;


class OtpStorageItem
{
    /**
     * @var null
     */
    public $mobile;
    /**
     * @var null
     */
    public $password;
    /**
     * @var null
     */
    public $sentAt;
    /**
     * @var null
     */
    public $expiresAt;

    /**
     * OtpStorageItem constructor.
     * @param $mobile
     * @param $password
     * @param null $sentAt
     * @param null $expiresAt
     */
    public function __construct($mobile = null, $password = null, $sentAt = null, $expiresAt = null)
    {
        $this->mobile = $mobile;
        $this->password = $password;
        $this->sentAt = $sentAt;
        $this->expiresAt = $expiresAt;
    }

    /**
     * Sometimes it is necessary to create a object of this class using json_decode result,
     * in this situations we can use this method to create object in a faster way.
     *
     * @param \stdClass $stdObject
     * @return OtpStorageItem
     */
    public static function createFromStdObject(\stdClass $stdObject)
    {
        $item = new self();
        foreach (get_object_vars($stdObject) as $key => $value) {
            $item->{$key} = $value;
        }
        return $item;
    }

    /**
     * This magic method is implemented to be able to store this object as string.
     * @return string
     */
    function __toString()
    {
        return json_encode($this);
    }

    /**
     * Check item expiresAt attribute and return true or false.
     * @return bool
     */
    public function isExpired()
    {
        if (!$this->expiresAt) {
            // if expiresAt is not set, we assume item is permanent.
            return false;
        }

        // now must be bigger thant expiration time,
        // otherwise it means item is expired
        return (time() > strtotime($this->expiresAt));
    }
}
