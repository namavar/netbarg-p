<?php
namespace App\Constants;

/**
 * Class Company.
 *
 * @package namespace App\Entities;
 */
class CompanyConstants
{
    const COMPANY_STATUS_BLACKLIST = 'blacklist';
    const COMPANY_STATUS_ACTIVE = 'active';
    const COMPANY_STATUS_DEACTIVATED = 'deactivated';

}
