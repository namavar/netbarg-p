<?php

namespace App\Constants;

/**
 * Class MessagesConstants.
 *
 * @package namespace App\Constants;
 */
class MessagesConstants
{
    const SUCCESS_INSERT_DATA = 'اطلاعات با موفقیت ثبت شد.';
    const SUCCESS_UPDATED_DATA = 'اطلاعات با موفقیت به روز رسانی شد.';
    const SUCCESS_DELETED_DATA = 'اطلاعات با موفقیت حذف شد.';
    const ERROR_INSERT_DATA = 'خطا در ثبت اطلاعات';
    const ACCESS_FORBIDDEN = 'شما دسترسی به این قسمت را ندارید.';
    const ROLE_NOT_VALID = 'سطح دسترسی مشخص شده اشتباه است.';
    const WRONG_CREDENTIAL = 'نام کاربری یا کلمه عبور اشتباه است';
    const WRONG_OTP_CREDENTIAL = "شماره مورد نظر یافت نشد";
    const LOGOUT_MESSAGE = 'شما با موفقیت از حساب کاربری خود خارج شدید.';
}
