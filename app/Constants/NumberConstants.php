<?php
namespace App\Constants;

/**
 * Class NumberConstants.
 *
 * @package namespace App\Constants;
 */
class NumberConstants
{
   const NUMBER_TRASH_ACTIVE = 0;
   const NUMBER_PAGE = 0;
   const NUMBER_PANG_NUMBER = 10;
   const NUMBER_ACTIVE = 1;
   const NOT_VERIFIED = 0;
   const PRIORITY = 1;

}
