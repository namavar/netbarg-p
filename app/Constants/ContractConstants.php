<?php

namespace App\Constants;

/**
 * Class Company.
 *
 * @package namespace App\Entities;
 */
class ContractConstants
{
    /**
     * list of Categorys
     */
    const CategoryConst = [

        [
            'id' => 15,
            'name' => 'Health',
            'label' => 'پزشکی و سلامت'
        ],

        [
            'id' => 8,
            'name' => 'Restaurant',
            'label' => 'رستوران و فست فود',
        ],

        [
            'id' => 6,
            'name' => 'Entertainment',
            'label' => 'تفریحی و ورزشی',
        ],

        [
            'id' => 7,
            'name' => 'Beauty',
            'label' => 'زیبایی و آرایشی',
        ],

        [
            'id' => 9,
            'name' => 'Art',
            'label' => 'فرهنگی و هنری',
        ],

        [
            'id' => 16,
            'name' => 'Education',
            'label' => 'آموزشی',
        ],

        [
            'id' => 5,
            'name' => 'Product',
            'label' => 'کالا',
        ],

/*        [
            'id' => 504,
            'name' => 'GiftCode',
            'label' => 'کد تخفیف',
        ],*/

        [
            'id' =>506,
            'name' => 'Service',
            'label' => 'خدمات',
        ]

    ];


    /**
     *All Constants For Every business Company
     */
    const ContractConstCategory = [


        15 => [
            'name' => 'Health',
            'label' => 'پزشکی و سلامت',
            'slug' => 'health',
            'required' => [
                [
                    'name' => 'certificateـofـbusinessـlicense',
                    'label' => 'گواهی جواز کسب',
                ],
                [
                    'name' => 'nationalـcard',
                    'label' => 'شناسنامه',
                ],
                [
                    'name' => 'birthـcertificate_Owner',
                    'label' => 'کارت ملی',
                ],
            ]
        ],



        8 => [
            'name' => 'Restaurant',
            'label' => 'رستوران و فست فود',
            'slug' => 'restaurant',
            'required' => [
                [
                    'name' => 'certificateـofـbusinessـlicense',
                    'label' => 'گواهی جواز کسب',
                ],
                [
                    'name' => 'nationalـcard',
                    'label' => 'شناسنامه',
                ],
                [
                    'name' => 'birthـcertificate_Owner',
                    'label' => 'کارت ملی',
                ],
            ]
        ],



        6 => [
            'name' => 'Entertainment',
            'label' => 'تفریحی و ورزشی',
            'slug' => 'entertainment',
            'required' => [ ]
        ],



        7 => [
            'name' => 'Beauty',
            'label' => 'زیبایی و آرایشی',
            'slug' => 'beauty',
            'required' => [ ]
        ],



        9 => [
            'name' => 'Art',
            'label' => 'فرهنگی و هنری',
            'slug' => 'art',
            'required' => [ ]
        ],



        16 => [
            'name' => 'Education',
            'label' => 'آموزشی',
            'slug' => 'education',
            'required' => [ ]
        ],



        5 => [
            'name' => 'Product',
            'label' => 'کالا',
            'slug' => 'product',
            'required' => [ ]
        ],


/*        504 =>[
            'name' => 'GiftCode',
            'label' => 'کد تخفیف',
            'slug' => 'giftCode ',
            'required' => [ ]
        ],*/


        506 => [
            'name' => 'Service',
            'label' => 'خدمات',
            'slug' => 'service ',
            'required' => [ ]
        ]

    ];


}
