<?php
namespace App\Constants;

/**
 * Class Company.
 *
 * @package namespace App\Entities;
 */
class RoleConstants
{
    
    const ROLE_ADMIN_ID = 1;
    const ROLE_USER_ID = 2;
    const ROLE_USER = ['1', '2', '3', '4', '5', '6'];
    const ROLE_COMPANY_CREATE = 3;
    const ROLE_USER_MANEG = 5;
    const ROLE_ADD_USER = [
        [
            'id' => 15,
            'name' => 'company user',
            'label' => 'ثبت کاربر'
        ],
        [
            'id' => 8,
            'name' => 'company',
            'label' => 'ثبت کردن کمپانی',
        ],
        [
            'id' => 6,
            'name' => '',
            'label' => 'ثبت deal',
        ],
        [
            'id' => 12,
            'name' => '',
            'label' => 'مدیریت',
        ]

    ];


}
