<?php

namespace App\Constants;

class DealsConstants
{
    const DealStatuses =
        [
            'DRAFT' => [
                'id' => 11,
                'alias' => 'DRAFT',
                'label' => 'پیش نویس'

            ],
            'UPCOMING' => [
                'id' => 1,
                'alias' => 'UPCOMING',
                'label' => 'در حال فعال شدن'
            ],
            'OPEN' => [
                'id' => 2,
                'alias' => 'OPEN',
                'label' => 'فعال'
            ],
            'TIPPED' => [
                'id' => 5,
                'alias' => 'TIPPED',
                'label' => 'به حد نصاب رسیده'
            ],
            'CLOSED' => [
                'id' => 6,
                'alias' => 'CLOSED',
                'label' => 'به پایان رسیده'
            ],
            'LIST_SENT' => [
                'id' => 14,
                'alias' => 'LIST_SENT',
                'label' => 'لیست ارسال شده'
            ],
            'REFUNDED' => [
                'id' => 7,
                'alias' => 'REFUNDED',
                'label' => 'باز پرداخت شده'
            ],
            'TRASH' => [
                'id' => 13,
                'alias' => 'TRASH',
                'label' => 'حذف شده'
            ],
            'PENDING_APPROVAL' => [
                'id' => 10,
                'alias' => 'PENDING_APPROVAL',
                'label' => 'در انتظار تایید'
            ],
        ];
}
