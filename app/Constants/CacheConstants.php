<?php
namespace App\Constants;

use App\Repositories\CompanyRepository;
use App\Repositories\DealRepository;

class CacheConstants
{
    /**
     * the keys of cache time are name of class and  methods in repository
     * maybe using other convention would be better !!!
     * values are time in minutes
     */

    const cacheTimes = [
        CompanyRepository::class . 'getUserCompanies' => 3,
        DealRepository::class . 'getDealsByFilter' => 3,
    ];

    public static function getCacheTime($cacheName)
    {
        $formattedCacheName =str_replace('::','',$cacheName);
        return self::cacheTimes[$formattedCacheName];
    }

}
