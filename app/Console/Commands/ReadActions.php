<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ReadActions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ReadActions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read Actions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $routes = app()->routes->getRoutes();

        foreach ($routes as $value) {
            $controller = null;
            if(isset($value->action['controller'])) {
                $controller = $value->action['controller'];
            }
            \App\Entities\Permission::firstOrCreate([
                'name' => $value->getPrefix()."/".$value->getActionMethod(),
                'uri' => $value->uri,
                'prefix' => $value->getPrefix(),
                'controller' => $controller,
                'guard_name' => 'api'
            ]);
        }
        echo "Done";
        return true;
    }
}
