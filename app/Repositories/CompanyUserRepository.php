<?php

namespace App\Repositories;

use App\Constants\NumberConstants;
use App\Entities\CompanyUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;


/**
 * Class CompanyUserRepository.
 *
 * @package namespace App\Repositories;
 */
class CompanyUserRepository
{
    private $model;

    public function __construct(CompanyUser $companyUser)
    {
        $this->model = $companyUser;
    }

    public function createCompanyUser($companyId,$userId)
    {
        return $this->model->create([
            'company_id' => $companyId,
            'user_id' => $userId,
        ]);
    }


    public function removeUserCompany($userId){
        return  $this->model->where([
            ['user_id', $userId]
        ])->delete();
    }

    public function getCompaniesUserByFilter($data, $user, $params)
    {
        $q = DB::table('company_users')
            ->join('users as user', 'user.id', '=', 'company_users.user_id')
            ->join('user_profiles as user_profile', 'user.id', '=', 'user_profile.user_id');


        $q->select([
            'company_users.id as companyUserId',
            'company_users.*',
            'user.*',
            'user_profile.*'
        ]);
//        $q =  $this->model->where('company_users.company_id', $companyId)
//            ->join('users as user', 'user.id', '=', 'company_users.user_id')
//            ->join('user_profiles as user_profile', 'user.id', '=', 'user_profile.user_id')
//            ->select([
//                'company_users.id as companyUserId',
//                'company_users.*',
//                'user.*',
//                'user_profile.*'
//            ])
//            ->get();

        $q->where('user.trash', NumberConstants::NUMBER_TRASH_ACTIVE);

        $q->where('company_users.company_id', $data['company_id']);

        // where not for owner company
        $q->whereNotIn('user.role_id', [3]);


        if (isset($params['phone']) && $params['phone'] != '') {
            $q->where('user_profile.phone', 'LIKE', '%' . $params['phone'] . '%');
        }

        if (isset($params['email']) && $params['email'] != '') {
            $q->where('user.email', 'LIKE', '%' . $params['email'] . '%');
        }

        $count = $q->distinct('company_users.id')->count('company_users.id');
        $q->groupBy('company_users.id');

        $page = NumberConstants::NUMBER_PAGE;
        $pageNumber = NumberConstants::NUMBER_PANG_NUMBER;
        if (isset($params['page'])) {
            $page = $params['page'];
        }

        if (isset($params['page_number'])) {
            $pageNumber = $params['page_number'];
        }

        $result = $q->skip($page * $pageNumber)
            ->take($pageNumber)->get();


        return [$count, $result];
    }



    public function showCompanyUser($id)
    {

        return  $this->model->where('company_users.user_id', $id)
            ->leftJoin('users as user', 'user.id', '=', 'company_users.user_id')
            ->leftJoin('user_profiles as user_profile', 'user.id', '=', 'user_profile.user_id')
            ->select([
                'company_users.id as companyUserId',
                'company_users.*',
                'user.*',
                'user_profile.*'
            ])
            ->get();
    }


    public function checkCompanyUser($userId, $companies)
    {
        $company = $this->model
            ->where('user_id', $userId)
            ->whereIn('company_id', $companies)->exists();
        return $company;
    }

    public function removeCompanyUser($userId, $companyId){
       return  $this->model->where([
            ['company_id' , $companyId],
            ['user_id', $userId]
        ])->delete();

    }

    public function getAllCompanies($user_id){
            return DB::table('company_users')->where('user_id', $user_id)
                ->select('company_id')->pluck('company_id')->toArray();
    }

    public function getAllUser($company_ids){
        return DB::table('company_users')->whereIn('company_id', $company_ids)
            ->select('user_id')->pluck('user_id')->toArray();

    }

}
