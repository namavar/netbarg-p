<?php

namespace App\Repositories;

use App\Entities\Setting;

/**
 * Class SettingRepository.
 *
 * @package namespace App\Repositories;
 */
class SettingRepository
{
    private $model;

    public function __construct(Setting $setting)
    {
        $this->model = $setting;
    }

    public function findWhere($data)
    {
        return $this->model->where($data)->get();
    }

    public function findByField($fieldName,$data)
    {
        return $this->model->where($fieldName,$data)->get();
    }
}
