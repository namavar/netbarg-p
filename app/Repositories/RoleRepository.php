<?php

namespace App\Repositories;

use App\Entities\Role;
use DB;
/**
 * Class RoleRepository.
 *
 * @package namespace App\Repositories;
 */
class RoleRepository
{
    private $model;

    public function __construct(Role $role)
    {
        $this->model = $role;
    }

    public function allRole()
    {
        return $this->model->orderBy('id')->get();
    }

    public function createRole($data)
    {
        return $this->model->create($data);
    }

    public function findRole($id)
    {
        return $this->model->find($id);
    }

    public function updateRole($data,$id)
    {
        return $this->model->find($id)->update($data);
    }

    public function destroyRole($id)
    {
        return $this->model->find($id)->update([
            'trash' => 1
        ]);
    }

    public function getPermisions()
    {
        return DB::table('permissions')
            ->pluck('name','id');
    }

}
