<?php

namespace App\Repositories;

use App\Entities\Permission;

/**
 * Class PermissionRepository.
 *
 * @package namespace App\Repositories;
 */
class PermissionRepository
{
    private $model;

    public function __construct(Permission $permission)
    {
        $this->model = $permission;
    }

    public function getAllPrefix()
    {
        return $this->model->select('prefix')->groupBy('prefix')->whereNotNull('prefix')->where([
            ['prefix','!=','api'],
            ['prefix','!=','oauth'],
            ['prefix','!=','api/admin'],
            ['prefix','!=','api/admin/role'],
            ['prefix','!=','api/admin/permissions'],
        ])->get();
    }

    public function getByPrefix($data)
    {
        return $this->model->with('role')->where(['prefix' => $data['prefix']])->get();
    }

}
