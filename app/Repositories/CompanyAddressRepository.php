<?php

namespace App\Repositories;

use App\Entities\CompanyAddress;

/**
 * Class CompanyAddressRepository.
 *
 * @package namespace App\Repositories;
 */
class CompanyAddressRepository
{
    private $model;

    public function __construct(CompanyAddress $companyAddress)
    {
        $this->model = $companyAddress;
    }

    public function createCompanyAddress($companyId, array $data,$zoneId)
    {
        return $this->model->create([
            'company_id' => $companyId,
            'phone' => $data['phone'],
            'address' => $data['address'],
            'zip_code' => $data['zip_code'],
            'city_id' => $data['city_id'],
            'zone_id' => $zoneId,
            'latitude' => $data['latitude'],
            'longitude' => $data['longitude'],
            'map_zoom_level' => $data['map_zoom_level'],
        ]);
    }

    public function updateCompanyAddress($companyId, array $data,$zoneId)
    {
        return $this->model->where('company_id',$companyId)->update([
            'phone' => $data['phone'],
            'address' => $data['address'],
            'zip_code' => $data['zip_code'],
            'city_id' => $data['city_id'],
            'zone_id' => $zoneId,
            'latitude' => $data['latitude'],
            'longitude' => $data['longitude'],
            'map_zoom_level' => $data['map_zoom_level'],
        ]);
    }
}
