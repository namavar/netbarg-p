<?php

namespace App\Repositories;


use App\Constants\NumberConstants;
use App\Entities\Contracts;
use Illuminate\Support\Facades\DB;


/**
 * Class ContractsRepository.
 *
 * @package namespace App\Repositories;
 */
class ContractsRepository
{
    private $model;

    public function __construct(Contracts $contracts)
    {
        $this->model = $contracts;
    }

    public function createContract(array  $data)
    {
        return $this->model->create([
            'company_name' => $data['company_name'],
            'company_id' => $data['company_id'],
            'national_code' => $data['national_code'],
            'email' => $data['email'],
            'contract_owner_name' =>  $data['contract_owner_name'],
            'sheba_number' =>  $data['sheba_number'],
            'company_share' =>  $data['company_share'],
            'name' =>  $data['name'],
            'bank_name' => $data['bank_name'],
            'card_number' => $data['card_number'],
            'contract_number' => $data['contract_number']

        ]);
    }

    public function updateContract(array $data, $contract_id )
    {
        return $this->model->where('id',$contract_id)->update([
            'company_name' => $data['company_name'],
            'national_code' => $data['national_code'],
            'email' => $data['email'],
            'contract_owner_name' =>  $data['contract_owner_name'],
            'sheba_number' =>  $data['sheba_number'],
            'company_share' =>  $data['company_share'],
            'name' =>  $data['name'],
            'bank_name' => $data['bank_name'],
            'card_number' => $data['card_number']
        ]);
    }


    public function getContractList($data){
        $q = DB::table('contracts')
                 ->where('contracts.company_id', $data['company_id']);

        $count = $q->distinct('contracts.id')->count('contracts.id');
        $q->where('trash' ,'=' , NumberConstants::NUMBER_TRASH_ACTIVE);
        $page = NumberConstants::NUMBER_PAGE;
        $pageNumber = NumberConstants::NUMBER_PANG_NUMBER;

        $result = $q->skip($page * $pageNumber)
            ->take($pageNumber)->get();

        return [$count, $result];

    }
    public function getContractCount(){
        return  $this->model->count();
    }


    public function showContract($id)
    {
        return  $this->model->where('id', $id)
            ->where('trash', NumberConstants::NUMBER_TRASH_ACTIVE)
            ->get();
    }

    public function destroyContract($id)
    {
        return $this->model->find($id)->update([
            'trash' => 1
        ]);
    }


}
