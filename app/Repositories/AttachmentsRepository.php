<?php

namespace App\Repositories;


use App\Constants\NumberConstants;
use App\Entities\Attachments;


/**
 * Class AttachmentsRepository.
 *
 * @package namespace App\Repositories;
 */
class AttachmentsRepository
{
    private $model;

    public function __construct(Attachments $attachments)
    {
        $this->model = $attachments;
    }

    public function create(array  $data, $id)
    {
        return $this->model->create([
                'identify' => $data['identify'],
                'filename' => $data['file_name'],
                'class'=> $data['class_name'],
                'dir' => $data['dir'],
                'foreign_id'=> $id,
                'priority' => NumberConstants::PRIORITY,
                'active' => NumberConstants::NUMBER_ACTIVE,
        ]);
    }
    public function update(array  $data, $id)
    {
        return $this->model->where('id', $id)->update([
            'identify' => $data['identify'],
            'filename' => $data['file_name'],
            'class'=> $data['class_name'],
            'dir' => $data['dir']
        ]);
    }

    public function destroyAttachments($id)
    {
        return $this->model->find($id)->update([
            'trash' => 1
        ]);
    }

    public function remove($id)
    {
        return $this->model->where('foreign_id', $id)->delete();
    }


}
