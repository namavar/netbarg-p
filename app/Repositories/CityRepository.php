<?php

namespace App\Repositories;

use App\Entities\City;

/**
 * Class CityRepository.
 *
 * @package namespace App\Repositories;
 */
class CityRepository
{
    private $model;

    public function __construct(City $city)
    {
        $this->model = $city;
    }

    public function getAllCities()
    {
        return $this->model->select('id', 'name')->get();
    }
}
