<?php

namespace App\Repositories;

use App\Constants\CacheConstants;
use App\Constants\DealsConstants;
use App\Entities\Deal;
use Illuminate\Cache\Repository as Cache;

/**
 * Class DealRepository.
 *
 * @package namespace App\Repositories;
 */
class DealRepository
{
    private $model;
    private $cache;

    public function __construct(Deal $deal, Cache $cache)
    {
        $this->model = $deal;
        $this->cache = $cache;
    }

    public function countDealForCompany($companyId)
    {
        return $this->model->where('company_id', $companyId)->count();
    }

    public function getDealsByFilter($user, $params)
    {
        $result = $this->cache->remember(__METHOD__ . md5(serialize($params).serialize($user->id)), CacheConstants::getCacheTime(__METHOD__), function () use ($user, $params) {
            $q = $this->model;
            $q = $q->select([
                'deals.id',
                'deals.name',
                'deals.original_price',
                'deals.discounted_price',
                'deals.discount_percentage',
                'deals.start_date',
                'deals.end_date',
                'deals.coupon_start_date',
                'deals.coupon_end_date',
                'deals.deal_user_count',
                'deals.has_property',
                'deals.is_postal',
                'deals.deal_status_id',
            ]);

            $q = $q->join('companies', 'companies.id', '=', 'deals.company_id');
            $q = $q->join('company_users', 'company_users.company_id', '=', 'companies.id');
            $q = $q->join('users', 'users.id', '=', 'company_users.user_id');
            $q = $q->where('users.id', $user->id);

            $q = $q->where('deals.trash', 0);

            $q = $q->with('status:id,name');
            $q = $q->with('dealCity.city:id,name');
            $q = $q->with('dealCategory.category:id,name');

            if (isset($params['name']) && $params['name'] != '') {
                $q->where('deals.name', 'like', '%' . $params['name'] . '%');
            }

            if (isset($params['status']) && $params['status'] != '') {
                $status = $params['status'];
                $q = $q->whereIn('deals.deal_status_id',$status);
            }else{
                $q = $q->whereNotIn('deals.deal_status_id',[
                    DealsConstants::DealStatuses['REFUNDED']['id'],
                    DealsConstants::DealStatuses['TRASH']['id'],
                ]);
            }

            $page = 0;
            $pageNumber = 10;

            if (isset($params['page'])) {
                $page = $params['page'];
            }

            if (isset($params['page_number'])) {
                $pageNumber = $params['page_number'];
            }

            $sorting = isset($params['sorting']) ? $params['sorting'] : [];
            if (isset($sorting['strategy'])) {
                $strategy = isset($sorting['strategy']) ? strtolower($sorting['strategy']) : 'asc';
                $q->orderBy('deals.' . $sorting['name'], $strategy);
            }

            $count = $q->count('deals.id');

            $result = $q->skip($page * $pageNumber)->take($pageNumber)->get();

            return [$count, $result];
        });

        return $result;
    }
}
