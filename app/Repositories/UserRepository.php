<?php

namespace App\Repositories;

use App\Constants\NumberConstants;
use App\Constants\RoleConstants;
use App\Entities\User;
use Hash;

/**
 * Class UserRepository.
 *
 * @package namespace App\Repositories;
 */
class UserRepository
{
    private $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function createUser(array $data)
    {
        return $this->model->create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role_id' => $data['role_id'],
            'active' => NumberConstants::NUMBER_ACTIVE
        ]);
    }


    public function updateUser(array $data, $userId)
    {
        $updateData = [
            'role_id' => $data['role_id'],
            'active' => $data['active']
        ];
        if(isset($data['password'])){
            $updateData['password'] = Hash::make($data['password']);
        }

        return $this->model->where('id',$userId)->update($updateData);
    }

    public function destroyUser($id)
    {
        return $this->model->find($id)->update([
            'role_id' => RoleConstants::ROLE_USER_ID
        ]);
    }

    public function getUserByEmail($email)
    {
        return $this->model->where('email',$email)->first();
    }

    public function checkRole($user_id){
        return $this->model->where('id', $user_id)->first()->role_id;
    }

}
