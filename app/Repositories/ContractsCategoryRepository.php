<?php

namespace App\Repositories;

use App\Entities\ContractCategory;


/**
 * Class ContractsCategoryRepository.
 *
 * @package namespace App\Repositories;
 */
class ContractsCategoryRepository
{
    private $model;

    public function __construct(ContractCategory $contractsCategory)
    {
        $this->model = $contractsCategory;
    }

    public function createContractCategory($contract_id,  $category_id)
    {
        return $this->model->create([
            'contract_id' => $contract_id,
            'category_id' => $category_id
        ]);
    }


}
