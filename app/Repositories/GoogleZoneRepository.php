<?php

namespace App\Repositories;

use App\Entities\GoogleZone;

/**
 * Class GoogleZoneRepository.
 *
 * @package namespace App\Repositories;
 */
class GoogleZoneRepository
{
    private $model;

    public function __construct(GoogleZone $googleZone)
    {
        $this->model = $googleZone;
    }

    public function createGoogleZone($data)
    {
        return $this->model->Create([
            'name' => $data['zone'],
            'latitude' => $data['latitude'],
            'longitude' => $data['longitude']
        ]);
    }
}
