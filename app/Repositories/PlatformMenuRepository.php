<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Entities\PlatformMenu;

/**
 * Class PlatformMenuRepository.
 *
 * @package namespace App\Repositories;
 */
class PlatformMenuRepository
{
    private $model;

    public function __construct(PlatformMenu $platformMenu)
    {
        $this->model = $platformMenu;
    }

    public function createPlatformMenu($data)
    {
        return $this->model->create([
            'name'          => $data['name'],
            'alias'         => $data['alias'],
            'parent_id'     => $data['parent_id'],
            'url'           => $data['url'],
            'icon'          => $data['icon'],
            'permission_id' => $data['permission_id'],
        ]);
    }

    public function getMenus($params){
        $page = 0;
        $pageNumber = 10;

        if (isset($params['page'])) {
            $page = $params['page'];
        }

        if (isset($params['page_number'])) {
            $pageNumber = $params['page_number'];
        }

        return $this->model
            ->where('platform_menus.parent_id', null)
            ->with('firstChildren')
            ->skip($page * $pageNumber)
            ->take($pageNumber)
            ->orderBy('id','desc')
            ->get();
    }

    public function updatePlatformMenu($data,$id){
        return $this->model->where('id', $id)->update($data);
    }

    public function getMenusWithUserRole($roleId){
      $query = DB::table('role_has_permissions')
        ->join('platform_menus', 'platform_menus.permission_id', '=', 'role_has_permissions.permission_id')
        ->where([
            ['role_has_permissions.role_id', $roleId]
        ])
        ->pluck('id','platform_menus.id');

        return $this->model
            ->whereIn('platform_menus.id', $query)
            ->with(['firstParent'])
            ->get();
    }

    public function getParents(){
        return $this->model
            ->where('platform_menus.parent_id','=', null)
            ->pluck('name','id');
    }
}
