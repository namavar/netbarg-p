<?php

namespace App\Repositories;


use App\Constants\DealsConstants;
use App\Entities\DealStatus;

class DealStatusRepository
{
    private $model;

    public function __construct(DealStatus $model)
    {
        $this->model = $model;
    }

    public function getAllStatus()
    {
        return $this->model->whereNotIn('id',[
            DealsConstants::DealStatuses['REFUNDED']['id'],
            DealsConstants::DealStatuses['TRASH']['id'],
        ])->select([
            'id',
            'name',
            'label'
        ])->get();
    }

}
