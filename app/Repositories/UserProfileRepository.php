<?php

namespace App\Repositories;

use App\Constants\NumberConstants;
use App\Entities\UserProfile;

/**
 * Class UserProfileRepository.
 *
 * @package namespace App\Repositories;
 */
class UserProfileRepository
{
    private $model;

    public function __construct(UserProfile $userProfile)
    {
        $this->model = $userProfile;
    }

    public function createUserProfile($userId, array $data )
    {

        return $this->model->create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'gender_id' => isset($data['gender_id'])?(int)$data['gender_id']:0,
            'state_id' => isset($data['state_id'])?(int)$data['state_id']:0,
            'city_id' => isset($data['city_id'])?(int)$data['city_id']:0,
            'national_code' => isset($data['national_code'])?$data['national_code']:'',
            'user_id' => $userId
//            'verified_phone' => 1
        ]);
    }
    public function updateUserProfile($userId, array $data)
    {
        return $this->model->where('user_id',$userId)->update([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'gender_id' => $data['gender_id'],
            'state_id' => $data['state_id'],
            'city_id' => $data['city_id'],
        ]);
    }


    public function getUserPhone($data)
    {
        return $this->model->where([
            'phone' => $data['phone'],
            'verified_phone' => NumberConstants::NUMBER_ACTIVE
        ])->first();
    }

    public function findWhere($data)
    {
        return $this->model->where($data);
    }

    public function checkPhone($data, $userId){

        $user = $this->model->where([
            'user_id' => $userId,
        ])->first();

       if(isset($user['phone']) &&  $data['phone'] == $user['phone']){
           return true;
       }else{
           return  $this->model->where('user_id',$userId)->update([
               'phone' => $data['phone'],
               'verified_phone' => NumberConstants::NOT_VERIFIED
           ]);
       }

    }

    public function addPhone($phone,$userId)
    {
        return $this->model->where('user_id',$userId)->update([
            'phone' => $phone
        ]);
    }

    public function verifyPhone($userId)
    {
        return $this->model->where('user_id',$userId)->update([
            'verified_phone' => 1
        ]);
    }
}
