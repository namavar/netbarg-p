<?php

namespace App\Repositories;

use App\Entities\Zone;

/**
 * Class ZoneRepository.
 *
 * @package namespace App\Repositories;
 */
class ZoneRepository
{
    private $model;

    public function __construct(Zone $zone)
    {
        $this->model = $zone;
    }

    public function getZones($data)
    {
        return $this->model->where([
                ['name', 'like', '%' . $data['name'] . '%']
        ])->select('id','name')->get();
    }

    public function findZone($data)
    {
        return $this->model->where([
            'name' => $data['zone'],
        ])->first();
    }

    public function createZone($data,$googleZoneId)
    {
        return $this->model->create([
            'name' => $data['zone'],
            'google_zone_id' => $googleZoneId
        ]);
    }
}
