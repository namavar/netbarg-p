<?php

namespace App\Repositories;

use App\Constants\CacheConstants;
use App\Constants\CompanyConstants;
use Illuminate\Support\Facades\DB;
use App\Entities\Company;
use Illuminate\Cache\Repository as Cache;

/**
 * Class CompanyRepository.
 *
 * @package namespace App\Repositories;
 */
class CompanyRepository
{
    private $model;

    private $cache;

    public function __construct(Company $company, Cache $cache)
    {
        $this->model = $company;
        $this->cache = $cache;
    }

    public function createCompany(array $data, $userId)
    {
        $company = new Company();
        $company->name = $data['name'];
        $company->description = $data['description'];
        $company->short_description = $data['short_description'];
        if (isset($data['website'])) $company->website = $data['website'];
        $company->created_by = $userId;
        $company->sale_agent_phone = $data['sale_agent_phone'];
        $company->show_phone = 1;
        $company->active = 0;
        $company->is_discount = $data['is_discount'];
        $company->has_sellercenter = $data['has_sellercenter'];
        $company->save();
        return $company;

    }

    public function getCompanyUserCount($companyId, $userId)
    {
        $company = $this->model->with([
            'user' => function ($query) use ($userId) {
                $query->where('user_id', $userId);
            }])
            ->where('id', $companyId)->count();
        return $company;
    }

    public function updateCompany(array $data, $id, $userId, $hasDeals = true)
    {
        $updateData = [
            'name' => $data['name'],
            'description' => $data['description'],
            'short_description' => $data['short_description'],
            'created_by' => $userId,
            'sale_agent_phone' => $data['sale_agent_phone'],
            'show_phone' => 1,
            'active' => 0,
        ];
        if (isset($data['website'])) $updateData['website'] = $data['website'];
        if (!$hasDeals) {
            $updateData['is_discount'] = $data['is_discount'];
            $updateData['has_sellercenter'] = $data['has_sellercenter'];
        }

        $model = $this->model->find($id);

        return $model->update($updateData);
//        return $this->model->where('id', $id)->update($updateData);
    }


    public function getCompaniesByFilter($user, $params, $isAdmin)
    {
        $q = DB::table('companies')
            ->join('company_addresses as ca', 'ca.company_id', '=', 'companies.id')
            ->leftJoin('attachments', function ($join) {
                $join->on('attachments.foreign_id', '=', 'companies.id')
                    ->where('attachments.class', 'company');
            });

        $q->select([
            'companies.*',
            'ca.address',
            'attachments.dir'
        ]);

        $q->where('companies.trash', 0);

        if (isset($params['id']) && $params['id'] != '') {
            $q->where('companies.id', $params['id']);
        }

        if (isset($params['name']) && $params['name'] != '') {
            $q->where('companies.name', 'LIKE', '%' . $params['name'] . '%');
        }

        if (!$isAdmin) {
            $q->join('company_users', function ($join) use ($user) {
                $join->on('company_users.company_id', '=', 'companies.id')
                    ->where('company_users.user_id', '=', $user->id);
            });
        } else {
            $q->addSelect('users.email');
            $q->join('company_users', 'company_users.company_id', '=', 'companies.id');
            $q->join('users', 'company_users.user_id', '=', 'users.id');

            //only admin users can search by email
            if (isset($params['email']) && $params['email'] != '') {
                $q->where('users.email', 'LIKE', '%' . $params['email'] . '%');
            }

            //only admin users can search by email
            if (isset($params['status']) && $params['status'] != '') {
                if ($params['status'] == CompanyConstants::COMPANY_STATUS_ACTIVE) {
                    $q->where('companies.active', 1);
                    $q->leftJoin('company_deal_blacklists', 'company_deal_blacklists.company_id', '=', 'companies.id');
                } elseif ($params['status'] == CompanyConstants::COMPANY_STATUS_DEACTIVATED) {
                    $q->where('companies.active', 0);
                    $q->leftJoin('company_deal_blacklists', 'company_deal_blacklists.company_id', '=', 'companies.id');
                } elseif ($params['status'] == CompanyConstants::COMPANY_STATUS_BLACKLIST) {
                    $q->join('company_deal_blacklists', 'company_deal_blacklists.company_id', '=', 'companies.id');
                }
            }
        }

        $count = $q->distinct('companies.id')->count('companies.id');
        $q->groupBy('companies.id');

        $page = 0;
        $pageNumber = 10;

        if (isset($params['page'])) {
            $page = $params['page'];
        }

        if (isset($params['page_number'])) {
            $pageNumber = $params['page_number'];
        }

        $sorting = isset($params['sorting']) ? $params['sorting'] : [];

        if (isset($sorting['column'])) {
            $strategy = isset($sorting['strategy']) ? strtolower($sorting['strategy']) : 'asc';
            $q->orderBy('companies.' . $sorting['column'], $strategy);
        }

//        $q->groupBy('companies.id');
        $result = $q->skip($page * $pageNumber)
            ->take($pageNumber)->get();

        return [$count, $result];
    }


    public function getAdminRelatedDataOfCompanies($isAdmin)
    {
        if (!$isAdmin) {
            return [0, 0, 0];
        }

        $q = DB::table('companies')
            ->select([
                DB::raw('sum(CASE WHEN companies.active =\'1\' AND company_deal_blacklists.id IS NULL THEN 1 ELSE 0 END) AS active_count'),
                DB::raw('sum(CASE WHEN companies.active = \'0\' AND company_deal_blacklists.id IS NULL THEN 1 ELSE 0 END) AS deactivated_count'),
                DB::raw('count(distinct CASE WHEN company_deal_blacklists.id IS NOT NULL THEN  company_deal_blacklists.company_id ELSE null END) AS blacklist_count'),
            ])
            ->leftJoin('company_deal_blacklists', 'company_deal_blacklists.company_id', '=', 'companies.id')
            ->where('companies.trash', 0);

        $activeCount = 0;
        $deactivatedCount = 0;
        $blacklistCount = 0;

        $sumResult = $q->first();
        if ($sumResult) {
            $activeCount = $sumResult->active_count;
            $deactivatedCount = $sumResult->deactivated_count;
            $blacklistCount = $sumResult->blacklist_count;
        }

        return [$activeCount, $deactivatedCount, $blacklistCount];

    }

    public function getCompany($id)
    {
        return $this->model->where('companies.id', $id)
            ->join('company_addresses as ca', 'ca.company_id', '=', 'companies.id')
            ->join('zones as z', 'z.id', '=', 'ca.zone_id')
            ->select([
                'companies.*',
                'ca.*',
                'z.name as zone'
            ])
            ->get();
    }

    public function destroyCompany($id)
    {
        return $this->model->find($id)->update([
            'trash' => 1
        ]);
    }

    public function getUserCompanies($user)
    {

        $result = $this->cache->remember(__METHOD__ . $user->id, CacheConstants::getCacheTime(__METHOD__) , function () use ($user) {
            $q = DB::table('companies')
                ->leftJoin('attachments', function ($join) {
                    $join->on('attachments.foreign_id', '=', 'companies.id')
                        ->where('attachments.class', 'company');
                });
            $q->select([
                'companies.*',
                'attachments.dir'
            ]);
            $q->where('companies.trash', 0);
            $q->join('company_users', function ($join) use ($user) {
                $join->on('company_users.company_id', '=', 'companies.id')
                    ->where('company_users.user_id', '=', $user->id);
            });

            $result = $q->get();
            return $result;
        });

        return $result;

    }
}
