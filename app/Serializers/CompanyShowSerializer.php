<?php

namespace App\Serializers;

use Illuminate\Support\Collection;

class CompanyShowSerializer implements SerializerInterface
{
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function serialize()
    {
        $data = $this->collection->flatMap(function ($company, $key) {
            return [
                'id' => $company->id,
                'name' => $company->name,
                'description' => $company->description,
                'shortDescription' => $company->short_description,
                'website' => $company->website,
                'saleAgentPhone' => $company->sale_agent_phone,
                'showPhone' => $company->show_phone,
                'isDiscount' => $company->is_discount,
                'hasSellerCenter' => $company->has_sellercenter,
                'isActive' => $company->active,
                'address' => [
                    'id' => $company->id,
                    'phone' => $company->phone,
                    'address' => $company->address,
                    'zipCode' => $company->zip_code,
                    'cityId' => $company->city_id,
                    'zone' => $company->zone,
                    'latitude' => $company->latitude,
                    'longitude' => $company->longitude,
                    'mapZoomLevel' => $company->map_zoom_level,
                ],
            ];
        });

        return $data;
    }
}
