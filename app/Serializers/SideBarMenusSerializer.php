<?php

namespace App\Serializers;

use Illuminate\Support\Collection;

class SideBarMenusSerializer implements SerializerInterface
{
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function serialize()
    {
        $this->collection =$this->collection->groupBy(function ($item, $key) {
            $item = $item->toArray();
            if($item['first_parent']){
                return $item['first_parent']['name'];
            }else{
                return '';
            }
        });

        $data = $this->collection->flatMap(function ($menu, $key) {
            $menu = $menu->toArray();
            if(!$key){
                $menus = [];
                foreach ($menu as $key => $value) {
                    $value = [
                        'name'  => $value['name'],
                        'alias' => $value['alias'],
                        'url'   => $value['url'],
                        'icon'   => $value['icon']
                    ];

                    array_push($menus, $value);
                }
                $result = $menus;
            }else{
                $children = [];
                foreach ($menu as $key => $value) {
                    $value = [
                        'name'  => $value['name'],
                        'alias' => $value['alias'],
                        'url'   => $value['url'],
                        'icon'   => $value['icon']
                    ];

                    array_push($children, $value);
                }

                $result[] = [
                    'parent' => [
                        'name'  => $menu[0]['first_parent']['name'],
                        'alias' => $menu[0]['first_parent']['alias'],                        
                        'icon'  => $menu[0]['first_parent']['icon'],          
                    ],
                    'children'  => $children
                ];
            }

            return $result;
        });

        return $data;
    }
}
