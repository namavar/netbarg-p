<?php

namespace App\Serializers;


use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyIndexCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return
            $this->collection->map(function ($item){
                return[
                    'id' => $item->id,
                    'name' => $item->name,
                    'isDiscount' => $item->is_discount,
                    'address' => str_limit($item->address, 100),
                    'website' => $item->website,
                    'hasSellerCenter' => $item->has_sellercenter,
                    'showPhone' => $item->show_phone,
                    'isActive' => $item->active,
                    'email' => property_exists($item, 'email') ? $item->email : '',
                    'image' => $item->dir ? env('APP_URL') . $item->dir : '',
                ];
            });
    }

}
