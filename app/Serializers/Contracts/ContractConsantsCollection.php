<?php
namespace App\Serializers\Contracts;



use Illuminate\Http\Resources\Json\ResourceCollection;

class ContractConsantsCollection extends ResourceCollection
{

    public function toArray($request)
    {
        return
            $this->collection->map(function ($item){
                return[
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'label' => $item['label'],
                ];
            });
    }

}
