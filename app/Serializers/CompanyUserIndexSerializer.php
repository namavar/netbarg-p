<?php

namespace App\Serializers;

use Illuminate\Support\Collection;

class CompanyUserIndexSerializer implements SerializerInterface
{
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function serialize()
    {

        $data = $this->collection->map(function ($companyUser, $key) {
            return [
                'company_id' => $companyUser->company_id,
                'user_id' => $companyUser->user_id,
                'email' =>  $companyUser->email,
                'phone' => $companyUser->phone,
                'first_name' => $companyUser->first_name,
                'last_name' => $companyUser->last_name,
                'last_name' => $companyUser->last_name,
                'active' => $companyUser->active,
                'verified_phone' => $companyUser->verified_phone
            ];
        });

        return $data;
    }
}
