<?php

namespace App\Serializers;



/**
 * Class PermissionPresenter.
 *
 * @package namespace App\Presenters;
 */
interface SerializerInterface
{
    public function serialize();
}
