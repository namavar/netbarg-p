<?php

namespace App\Serializers;


use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCompaniesCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return
            $this->collection->map(function ($item){
                return[
                    'id' => $item->id,
                    'name' => $item->name,
                    'isDiscount' => $item->is_discount,
                    'hasSellerCenter' => $item->has_sellercenter,
                    'isActive' => $item->active,
                    'image' => $item->dir ? env('APP_URL') . $item->dir : '',
                    ];
            });
    }

}
