<?php

namespace App\Serializers;

use Illuminate\Support\Collection;

class CompanyIndexSerializer implements SerializerInterface
{
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function serialize()
    {

        $data = $this->collection->map(function ($company, $key) {
            return [
                'id' => $company->id,
                'name' => $company->name,
                'isDiscount' => $company->is_discount,
                'address' => str_limit($company->address, 100),
                'website' => $company->website,
                'hasSellerCenter' => $company->has_sellercenter,
                'showPhone' => $company->show_phone,
                'isActive' => $company->active,
                'email' => property_exists($company, 'email') ? $company->email : '',
                'image' => $company->dir ? env('APP_URL') . $company->dir : '',
            ];
        });

        return $data;
    }
}
