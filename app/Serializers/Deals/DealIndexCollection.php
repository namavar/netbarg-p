<?php

namespace App\Serializers\Deals;

use App\Serializers\Cities\CityIndexCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class DealIndexCollection extends ResourceCollection
{

    public function toArray($request)
    {
        return
            $this->collection->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                    'original_price' => $item->original_price,
                    'discounted_price' => $item->discounted_price,
                    'discount_percentage' => $item->discount_percentage,
                    'start_date' => $item->start_date,
                    'end_date' => $item->end_date,
                    'coupon_start_date' => $item->coupon_start_date,
                    'coupon_end_date' => $item->coupon_end_date,
                    'status' => $item->status->name,
                    'deal_user_count' => $item->deal_user_count,
                    'has_property' => $item->has_property,
                    'is_postal' => $item->is_postal,
                    'cities' => $item->dealCity->map(function ($item) {
                        return [
                            'id' => $item->city->id,
                            'name' => $item->city->name
                        ];
                    }),
                    'categories' => $item->dealCategory->map(function ($item) {
                        return [
                            'id' => $item->category->id,
                            'name' => $item->category->name
                        ];
                    }),
                ];
            });
    }

}
