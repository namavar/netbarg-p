<?php

namespace App\Serializers;

use Illuminate\Support\Collection;

class UserCompaniesSerializer implements SerializerInterface
{
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function serialize()
    {

        $data = $this->collection->map(function ($company, $key) {
            return [
                'id' => $company->id,
                'name' => $company->name,
                'isDiscount' => $company->is_discount,
                'hasSellerCenter' => $company->has_sellercenter,
                'isActive' => $company->active,
                'image' => $company->dir ? env('APP_URL') . $company->dir : '',
            ];
        });
        return $data;
    }
}
