<?php

namespace App\Serializers;

use Illuminate\Support\Collection;

class ContractShowSerializer implements SerializerInterface
{
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function serialize()
    {
        $data = $this->collection->flatMap(function ($contract, $key) {
            return [
                'id' => $contract->id,
                'company_id' => $contract->company_id,
                'user_id' => $contract->company_name,
                'email' =>  $contract->email,
                'contract_owner_name' => $contract->contract_owner_name,
                'national_code' => $contract->national_code,
                'name' => $contract->name,
                'bank_name' => $contract->bank_name,
                'card_number' => $contract->card_number,
                'company_share' => $contract->company_share,
                'sheba_number' => $contract->sheba_number,
            ];
        });

        return $data;
    }
}
