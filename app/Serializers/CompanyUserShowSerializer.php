<?php

namespace App\Serializers;

use Illuminate\Support\Collection;

class CompanyUserShowSerializer implements SerializerInterface
{
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function serialize()
    {
        $data = $this->collection->flatMap(function ($companyUser, $key) {
            return [
                'company_id' => $companyUser->company_id,
                'user_id' => $companyUser->User_id,
                'email' => $companyUser->email,
                'first_name' => $companyUser->first_name,
                'last_name' => $companyUser->last_name,
                'gender_id' => $companyUser->gender_id,
                'city_id' => $companyUser->city_id,
                'state_id' => $companyUser->state_id,
                'phone' => $companyUser->phone,
                'national_code' => $companyUser->national_code,
                'active' => $companyUser->active,
                'verified_phone' => $companyUser->verified_phone
            ];
        });

        return $data;
    }
}
