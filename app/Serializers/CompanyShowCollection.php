<?php

namespace App\Serializers;


use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyShowCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return
            $this->collection->flatMap(function ($item){
                return[
                    'id' => $item->id,
                    'name' => $item->name,
                    'description' => $item->description,
                    'shortDescription' => $item->short_description,
                    'website' => $item->website,
                    'saleAgentPhone' => $item->sale_agent_phone,
                    'showPhone' => $item->show_phone,
                    'isDiscount' => $item->is_discount,
                    'hasSellerCenter' => $item->has_sellercenter,
                    'isActive' => $item->active,
                    'address' => [
                        'id' => $item->id,
                        'phone' => $item->phone,
                        'address' => $item->address,
                        'zipCode' => $item->zip_code,
                        'cityId' => $item->city_id,
                        'zone' => $item->zone,
                        'latitude' => $item->latitude,
                        'longitude' => $item->longitude,
                        'mapZoomLevel' => $item->map_zoom_level,
                    ],
                ];
            });
    }

}
