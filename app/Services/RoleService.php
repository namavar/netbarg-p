<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use Spatie\Permission\Models\Role;

class RoleService extends BaseService
{
    protected $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function listRoles()
    {
        $roles = $this->repository->allRole();
        return $this->success($roles);
    }

    public function createRole($data)
    {
        $response = $this->repository->createRole($data);
        return $this->success($response);
    }

    public function findRole($id)
    {
        $response = $this->repository->findRole($id);
        return $this->success($response);
    }

    public function editRole($data,$id)
    {
        $response = $this->repository->updateRole($data,$id);
        return $this->success($response);
    }

    public function destroyRole($id)
    {
        $response = $this->repository->destroyRole($id);
        return $this->success($response);
    }

    public function addRolePermission($data)
    {
        $role = Role::find($data['role_id']);
        $response = $role->givePermissionTo($data['permissions']);
        return $this->success($response);
    }

    public function removeRolePermission($data)
    {
        $role = Role::find($data['role_id']);
        $response = $role->revokePermissionTo($data['permissions']);
        return $this->success($response);
    }

    public function getPermisions(){
        return $this->repository->getPermisions();
    }
}
