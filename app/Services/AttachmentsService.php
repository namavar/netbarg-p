<?php

namespace App\Services;


use App\Repositories\AttachmentsRepository;
use Illuminate\Support\Facades\DB;


class AttachmentsService extends BaseService
{
    /**
     * @var AttachmentsRepository
     */
    protected $attachmentsRepository;


    public function __construct(AttachmentsRepository $attachmentsRepository)
    {
        $this->attachmentsRepository = $attachmentsRepository;

    }
    public function uploads($data){
        $result['class_name'] = $this->getClass();
        $result['identify'] = uniqid();
        $result['file_name'] = $result['identify'].'.'.$data['file']->getClientOriginalExtension();
        $data['file']->move(public_path().'/files/', $result['file_name']);
        $result['dir'] = '/' . $result['class_name'] . '/' . $data['target_id'] . '/' . $result['file_name'];
        DB::beginTransaction();
        $file =  $this->attachmentsRepository->create($result, $data['target_id']);
        DB::commit();
        if($file){
            $data = [
                'status' => true,
                'id' => $file->id,
                'dir' => $file->dir

            ];
        }else{
            $data = [
                'status' => false,
            ];
        }

        return $this->success($data);


    }

    public function updateUploads($data, $id){

        $result['class_name'] = $this->getClass();
        $result['identify'] = uniqid();
        $result['file_name'] = $result['identify'].'.'.$data['file']->getClientOriginalExtension();
        $data['file']->move(public_path().'/files/', $result['file_name']);
        $result['dir'] = '/' . $result['class_name'] . '/' . $data['target_id'] . '/' . $result['file_name'];
        DB::beginTransaction();
       $file = $this->attachmentsRepository->update($result, $id);
        DB::commit();
        if($file){
            $data = [
                'status' => true,
            ];
        }else{
            $data = [
                'status' => false,
            ];
        }
        return $this->success($data);

    }

    public function destroyAttachments($id){
       return $this->attachmentsRepository->remove($id);
    }

    protected function getClass(){
        $action = app('request')->route()->getAction();
        $controllerAction = class_basename($action['controller']);
        $controller_array = explode('@', $controllerAction);
        $controller = str_replace('Controller', '', $controller_array[0]);
        return  strtolower($controller);
    }


}
