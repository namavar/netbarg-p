<?php
/**
 * Created by PhpStorm.
 * User: bahador
 * Date: 12/26/18
 * Time: 1:46 PM
 */

namespace App\Services;


class BaseService
{
    public function success($data = null,$message = ""){
        return [
            'status' => true,
            'message' => $message,
            'data' => $data
        ];
    }

    public function error($message,$data =  null)
    {
        return [
            'status' => false,
            'message' => $message,
            'data' => $data
        ];
    }
}
