<?php

namespace App\Services;


use App\Constants\ContractConstants;
use App\Constants\NumberConstants;
use App\Constants\RoleConstants;
use App\Repositories\ContractsCategoryRepository;
use App\Repositories\ContractsRepository;
use App\Serializers\ContractIndexSerializer;
use App\Serializers\ContractIndexSerializerCollection;
use App\Serializers\Contracts\DealIndexCollection;
use App\Serializers\ContractShowSerializer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ContractsService extends BaseService
{
    /**
     * @var ContractsRepository
     */
    protected $contractRepository;
    protected $contractCategoryRepository;
    protected $authorizationManagerService;
    protected $attachmentsService;





    public function __construct(ContractsRepository $contractRepository, ContractsCategoryRepository $contractCategoryRepository,
                                AuthorizationManagerService $authorizationManagerService, AttachmentsService $attachmentsService)
    {
        $this->contractRepository = $contractRepository;
        $this->contractCategoryRepository = $contractCategoryRepository;
        $this->authorizationManagerService = $authorizationManagerService;
        $this->attachmentsService = $attachmentsService;

    }
    public function getContract($data){

        list($count, $result) = $this->contractRepository->getContractList($data);

        $companyUserIndexSerializer = new ContractIndexSerializer($result);
        $data = [
            'count' => $count,
            'company_user' => $companyUserIndexSerializer->serialize()
        ];
        return $this->success($data);
    }


    public function createContract($data, $user)
    {
        $date  = \Morilog\Jalali\CalendarUtils::toJalali('20'.(int)date('y'),(int)date('m'), (int)date('d'));
        if ($this->authorizationManagerService->UserCanAccessCompany($data['company_id'], $user['id'], $user['role_id'])) {
            try {
                DB::beginTransaction();
                $contractNumberCounter = $this->contractRepository->getContractCount();
                $data['contract_number'] = $date[0]. '-' . $date[1] . '-' . $date['2'].'-'.str_pad($contractNumberCounter++, 3, "0", STR_PAD_LEFT);
                $contract  =$this->contractRepository->createContract($data);
                $this->contractCategoryRepository->createContractCategory($contract['id'], $data['category_id']);
                DB::commit();
                return $this->success(null,Lang::get('response.success_insert_data'));

            } catch (\Exception $e) {
                DB::rollBack();
                return $this->error(Lang::get('response.error_insert_data'));
            }
        } else {
            return $this->error(Lang::get('response.access_forbiden'));
        }
    }

    public function updateContract($data, $contract_id, $user)
    {
        if ($this->authorizationManagerService->UserCanAccessCompany($data['company_id'], $user['id'], $user['role_id'])) {
            try {
                DB::beginTransaction();
                $this->contractRepository->updateContract($data, $contract_id);
                DB::commit();
                return $this->success(null,Lang::get('response.success_update_data'));
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->error(Lang::get('response.error_update_data'));
            }
        } else {
            return $this->error(Lang::get('response.access_forbiden'));
        }
    }

    public function ShowContract($data ,$contract_id , $user)
    {

        if ($this->authorizationManagerService->UserCanAccessCompany($data['company_id'], $user['id'], $user['role_id'])) {
            $contractShow = $this->contractRepository->showContract($contract_id);
            $contractShowSerializer = new ContractShowSerializer($contractShow);
            $data =  $contractShowSerializer->serialize();

            return $this->success($data);
        } else {
            return $this->error(Lang::get('response.access_forbiden'));
        }

    }

    public function removeContract($data,$contract_id, $user){
        if ($this->authorizationManagerService->UserCanAccessCompany($data['company_id'], $user['id'], $user['role_id'])) {
            $this->contractRepository->destroyContract($contract_id);
            $this->attachmentsService->destroyAttachments($contract_id);
            return $this->success(null, Lang::get('response.success_delete_data'));
        } else {
            return $this->error(Lang::get('response.access_forbiden'));
        }

    }


    public function constantsCategories ()
    {
        $category = collect(ContractConstants::CategoryConst);
        $category = new DealIndexCollection($category);
        $data = [
            'categories' => $category
        ];
        return $this->success($data);
    }


    public function getRequiredCategory ($id)
    {
        $category = collect(ContractConstants::ContractConstCategory[$id]);
        return $this->success($category);
    }

    public function getRole(){
        $role = collect(RoleConstants::ROLE_ADD_USER);
        return $this->success($role);
    }




}
