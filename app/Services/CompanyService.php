<?php

namespace App\Services;

use App\Constants\NumberConstants;
use App\Constants\RoleConstants;
use App\Entities\User;
use App\Repositories\CityRepository;
use App\Repositories\CompanyAddressRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\CompanyUserRepository;
use App\Repositories\DealRepository;
use App\Repositories\GoogleZoneRepository;
use App\Repositories\UserRepository;
use App\Repositories\ZoneRepository;
use App\Serializers\CompanyIndexCollection;
use App\Serializers\CompanyShowCollection;
use App\Serializers\UserCompaniesCollection;
use DB;
use Illuminate\Support\Facades\Lang;

class CompanyService extends BaseService
{
    /**
     * @var CompanyRepository
     */
    protected $repository;
    protected $companyAddressRepository;
    protected $companyUserRepository;
    protected $cityRepository;
    protected $zoneRepository;
    protected $googleZoneRepository;
    protected $authorizationManagerService;
    protected $dealRepository;
    protected $userRepository;

    public function __construct(CompanyRepository $repository, CompanyAddressRepository $companyAddressRepository,
                                CompanyUserRepository $companyUserRepository, CityRepository $cityRepository,
                                ZoneRepository $zoneRepository, GoogleZoneRepository $googleZoneRepository,
                                AuthorizationManagerService $authorizationManagerService, DealRepository $dealRepository,
                                UserRepository $userRepository)
    {
        $this->repository = $repository;
        $this->companyAddressRepository = $companyAddressRepository;
        $this->companyUserRepository = $companyUserRepository;
        $this->cityRepository = $cityRepository;
        $this->zoneRepository = $zoneRepository;
        $this->googleZoneRepository = $googleZoneRepository;
        $this->authorizationManagerService = $authorizationManagerService;
        $this->dealRepository = $dealRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     * @param array $filters
     * @param boolean $fetchAllCompanies
     * @return  array
     */
    public function getCompaniesByFilter($user, $filters, $isAdmin)
    {
        //these are the only params that user can set
        $allowedFilterParams = ['id', 'name', 'email', 'is_discount', 'sorting', 'page', 'status'];
        @$params = array_intersect_key($filters, array_flip($allowedFilterParams));

        list($count, $result) = $this->repository->getCompaniesByFilter($user, $params, $isAdmin);

        list($activeCount, $deactivatedCount, $blacklistCount) = $this->repository->getAdminRelatedDataOfCompanies($isAdmin);

        //here we are serializing data to our customized one
        //$companyIndexSerializer = new CompanyIndexSerializer($result);
        $companyIndexSerializer = new CompanyIndexCollection($result);

        $data = [
            'count' => $count,
            'companies' => $companyIndexSerializer,
            'adminRelatedData' => [
                'isAdmin' => $isAdmin,
                'activeCount' => intval($activeCount),
                'deactivatedCount' => intval($deactivatedCount),
                'blacklistCount' => intval($blacklistCount),
            ]
        ];
        return $this->success($data);
    }

    public function createCompany($data, $user)
    {
        try {
            DB::beginTransaction();
            $company = $this->repository->createCompany($data, $user->id);
            $this->companyUserRepository->createCompanyUser($company->id, $user->id);
            $all_company = $this->companyUserRepository->getAllCompanies($user->id);
            $all_user = $this->companyUserRepository->getAllUser($all_company);
            foreach ($all_user as $value){
               $role = $this->userRepository->checkRole($value);
               if($role == RoleConstants::ROLE_USER_MANEG){
                   $this->companyUserRepository->createCompanyUser($company->id, $value);
               }
            }
            $zone = $this->zoneRepository->findZone($data);
            if (!$zone) {
                $googleZone = $this->googleZoneRepository->createGoogleZone($data);
                $zone = $this->zoneRepository->createZone($data, $googleZone->id);
            }
            $this->companyAddressRepository->createCompanyAddress($company->id, $data, $zone->id);
            DB::commit();
            return $this->success([
                'companyId' => $company->id
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error(Lang::get('response.error_insert_data'));
        }
    }

    public function updateCompany($data, $id, $user)
    {
        if ($this->authorizationManagerService->UserCanAccessCompany($id,$user->id,$user->role)) {
            $dealCount = $this->dealRepository->countDealForCompany($id);
            $hasDeals = false;
            if ($dealCount != 0) {
                $hasDeals = true;
            }
            try {
                DB::beginTransaction();
                $this->repository->updateCompany($data, $id, $user->id, $hasDeals);
                $zone = $this->zoneRepository->findZone($data);
                if (!$zone) {
                    $googleZone = $this->googleZoneRepository->createGoogleZone($data);
                    $zone = $this->zoneRepository->createZone($data, $googleZone->id);
                }
                $this->companyAddressRepository->updateCompanyAddress($id, $data, $zone->id);
                DB::commit();
                return $this->success([
                    'companyId' => $id
                ]);
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->error(Lang::get('response.success_insert_data'));
            }
        } else {
            return $this->error(Lang::get('reponse.access_forbiden'));
        }
    }

    public function getCompany($id,$user)
    {
        if ($this->authorizationManagerService->UserCanAccessCompany($id,$user->id,$user->role)) {
            $company = $this->repository->getCompany($id);
            $dealCount = $this->dealRepository->countDealForCompany($id);
            //$companyShowSerializer = new CompanyShowSerializer($company);
            $companyShowSerializer = new CompanyShowCollection($company);
            $data = [
                'company' => $companyShowSerializer,
                'dealCount' => $dealCount
            ];
            return $this->success($data);
        } else {
            return $this->error(Lang::get('reponse.access_forbiden'));
        }
    }

    public function removeCompany($id,$user)
    {
        if ($this->authorizationManagerService->UserCanAccessCompany($id,$user->id,$user->role)) {
            $this->repository->destroyCompany($id);
            return $this->success(null, Lang::get('response.success_delete_data'));
        } else {
            return $this->error(Lang::get('reponse.access_forbiden'));
        }
    }

    public function getUserCompanies($user, $isAdmin)
    {
        if ($isAdmin) {
            return $this->success(null);
        }
        $result = $this->repository->getUserCompanies($user);
        //$userCompaniesSerializer = new UserCompaniesSerializer($result);
        $userCompaniesSerializer = new UserCompaniesCollection($result);
        $data = [
            'count' => count($result),
            'companies' => $userCompaniesSerializer,
        ];
        return $this->success($data);

    }
}

