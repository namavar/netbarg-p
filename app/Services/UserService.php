<?php

namespace App\Services;

use App\Constants\MessagesConstants;
use App\Constants\RoleConstants;
use App\Repositories\UserProfileRepository;
use App\Repositories\UserRepository;
use DB;
use Auth;
use Illuminate\Support\Facades\Lang;
use Lcobucci\JWT\Parser;

class UserService extends BaseService
{
    /**
     * @var UserRepository
     */
    protected $repository;
    protected $userProfileRepository;
    protected $otpService;

    /**
     *
     * @param UserRepository $repository
     * @param UserProfileRepository $userProfileRepository
     * @param OtpService $otpService
     */
    public function __construct(UserRepository $repository, UserProfileRepository $userProfileRepository, OtpService $otpService)
    {
        $this->repository = $repository;
        $this->userProfileRepository = $userProfileRepository;
        $this->otpService = $otpService;
    }

    public function login($data)
    {
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $user = Auth::getLastAttempted();
            if (array_key_exists($user->role_id, RoleConstants::ROLE_USER)) {
                if ($user->UserProfile()->first()->verified_phone) {
                    $user = Auth::user();
                    $token = $user->createToken('Platform')->accessToken;

                    return $this->success([
                        'userName' => $user->UserProfile()->first()->first_name . " " . $user->UserProfile()->first()->last_name,
                        'token' => $token
                    ]);
                } else {
                    return $this->success([
                        'email' => $user->email,
                        'mobile' => $user->UserProfile()->first()->phone
                    ]);
                }
            } else {
                return $this->error(Lang::get('response.access_forbiden'));
            }
        } else {
            return $this->error(Lang::get('response.error_username_password'));
        }
    }

    public function createNewUser($data)
    {
        if (isset($data['confirm_code'])) {
            if (isset($data['g-recaptcha-response'])) {
                $verify = $this->otpService->verify($data['phone'], $data['confirm_code'], $data['g-recaptcha-response']);
            } else {
                $verify = $this->otpService->verify($data['phone'], $data['confirm_code']);
            }
            if ($verify['status']) {
                try {
                    DB::beginTransaction();
                    $data['role_id'] = RoleConstants::ROLE_COMPANY_CREATE;
                    $user = $this->repository->createUser($data);
                    $this->userProfileRepository->createUserProfile($user->id, $data);
                    DB::commit();
                    $token = $user->createToken('Platform')->accessToken;
                    return $this->success([
                        'userName' => $data['first_name'] . " " . $data['last_name'],
                        'token' => $token
                    ]);
                } catch (\Exception $e) {
                    DB::rollBack();
                    return $this->error('خطا در ثبت اطلاعات');
                }
            } else {
                return $verify;
            }
        } else {
            $otpResponse = $this->otpService->send($data['phone']);
            return $otpResponse;
        }
    }

    public function otpLogin($data)
    {
        $userProfile = $this->userProfileRepository->getUserPhone($data);
        if (!$userProfile) {
            return $this->error(Lang::get('response.not_found_phone_number'));
        } else {
            $user = $userProfile->user()->first();
            if (array_key_exists($user->role_id, RoleConstants::ROLE_USER)) {
                if (isset($data['confirm_code'])) {
                    $verify = $this->otpService->verify($data['phone'], $data['confirm_code']);
                    if ($verify['status']) {
                        $token = $user->createToken('Platform')->accessToken;
                        $userName = $userProfile->first_name . " " . $userProfile->last_name;
                        return $this->success([
                            'userName' => $userName,
                            'token' => $token
                        ]);
                    } else {
                        return $verify;
                    }
                } else {
                    $otpResponse = $this->otpService->send($data['phone'], $userProfile->user_id);
                    return $otpResponse;
                }
            } else {
                return $this->error(Lang::get('response.access_forbiden'));
            }
        }
    }

    public function logout($data)
    {
        $value = $data->bearerToken();
        $id = (new Parser())->parse($value)->getHeader('jti');
        $token = $data->user()->tokens->find($id);
        $token->revoke();

        return $this->success([], Lang::get('response.logout_success'));
    }

    public function verifyPhoneOnLogin($data)
    {
        $user = $this->repository->getUserByEmail($data['email']);
        $userProfile = $user->UserProfile()->first();
        if (array_key_exists($user->role_id, RoleConstants::ROLE_USER) && $userProfile->verified_phone == 0) {
            if (isset($data['confirm_code'])) {
                $verify = $this->otpService->verify($data['phone'], $data['confirm_code']);
                if ($verify['status']) {
                    $this->userProfileRepository->verifyPhone($user->id);
                    $token = $user->createToken('Platform')->accessToken;
                    $userName = $userProfile->first_name . " " . $userProfile->last_name;
                    return $this->success([
                        'userName' => $userName,
                        'token' => $token
                    ]);
                } else {
                    return $verify;
                }
            } else {
                if (!isset($userProfile->phone)) {
                    $this->userProfileRepository->addPhone($data['phone'], $user->id);
                }
                $otpResponse = $this->otpService->send($data['phone'], $user->id);
                return $otpResponse;
            }
        } else {
            return $this->error(Lang::get('response.access_forbiden'));
        }
    }
}
