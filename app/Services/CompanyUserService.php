<?php

namespace App\Services;

use App\Constants\RoleConstants;
use App\Entities\CompanyUser;
use App\Repositories\CompanyUserRepository;
use App\Repositories\UserProfileRepository;
use App\Repositories\UserRepository;
use App\Serializers\CompanyUserIndexSerializer;
use App\Serializers\CompanyUserShowSerializer;
use Auth;
use DB;
use Illuminate\Support\Facades\Lang;

class CompanyUserService extends BaseService
{
    /**
     * @var CompanyUserRepository
     */
    protected $companyUserRepository;
    protected $authorizationManagerService;
    protected $usersRepository;
    protected $userProfileRepository;

    public function __construct(UserRepository $usersRepository, CompanyUserRepository $companyUserRepository,
                                AuthorizationManagerService $authorizationManagerService, UserProfileRepository $userProfileRepository)
    {
        $this->companyUserRepository = $companyUserRepository;
        $this->authorizationManagerService = $authorizationManagerService;
        $this->usersRepository = $usersRepository;
        $this->userProfileRepository = $userProfileRepository;
    }


    public function getCompanyUserByFilter($requestParams, $user, $filters)
    {
        //these are the only params that user can set
        $allowedFilterParams = ['phone', 'email', 'page', 'page_number'];
        @$params = array_intersect_key($filters, array_flip($allowedFilterParams));

        list($count, $result) = $this->companyUserRepository->getCompaniesUserByFilter($requestParams, $user, $params);


        $companyUserIndexSerializer = new CompanyUserIndexSerializer($result);
        $data = [
            'count' => $count,
            'company_user' => $companyUserIndexSerializer->serialize()
        ];
        return $this->success($data);

    }

    public function createCompanyUser($data, $user_id)
    {
        $role = array_key_exists((int)$data['role_id'], RoleConstants::ROLE_USER);
        if (!$role) {
            return $this->error(Lang::get('response.role_not_invalid'));
        }
        try {
            DB::beginTransaction();

            $user = $this->usersRepository->createUser($data);
            $this->userProfileRepository->createUserProfile($user->id, $data);
            if ($data['role_id'] == RoleConstants::ROLE_USER_MANEG) {
                $data['companies'] = $this->companyUserRepository->getAllCompanies($user_id);
            }
            foreach ($data['companies'] as $item) {
                $this->companyUserRepository->createCompanyUser((int)$item, $user->id);
            }
            DB::commit();
            return $this->success(null, Lang::get('response.success_insert_data'));
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error(Lang::get('response.error_insert_data'));
        }
    }

    public function updateCompanyUser($data, $userId, $user_login)
    {
        if ($this->authorizationManagerService->CompanyCanAccessUser($userId, $data['companies'], $user_login->role_id)) {
            $role = array_key_exists($data['role_id'], RoleConstants::ROLE_USER);
            if (!$role) {
                return $this->error(Lang::get('response.role_not_invalid'));
            }

            try {
                DB::beginTransaction();
                $this->usersRepository->updateUser($data, $userId);
                if (isset($data['phone'])) {
                    $this->userProfileRepository->checkPhone($data, $userId);
                }
                $this->userProfileRepository->updateUserProfile($userId, $data);
                if(isset($data['role_id']) && $data['role_id'] == RoleConstants::ROLE_USER_MANEG){
                    $data['companies'] = $this->companyUserRepository->getAllCompanies($user_login->id);
                    foreach ($data['companies'] as $item) {
                        $this->companyUserRepository->removeUserCompany($userId);
                        $this->companyUserRepository->createCompanyUser((int)$item, $userId);
                    }
                }else{
                    if(isset($data['companies'])){
                        foreach ($data['companies'] as $item) {
                            $this->companyUserRepository->createCompanyUser((int)$item, $userId);
                        }
                    }
                }
                DB::commit();
                return $this->success(null, Lang::get('response.success_update_data'));
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->error(Lang::get('get.error_update_data'));
            }
        } else {
            return $this->error(Lang::get('response.access_forbiden'));
        }

    }

    public function ShowCompanyUser($data, $userId, $roleId)
    {
        if ($this->authorizationManagerService->CompanyCanAccessUser($userId, [$data['company_id']], $roleId)) {
            $companyUserShow = $this->companyUserRepository->showCompanyUser($userId);
            $companyUserShowSerializer = new CompanyUserShowSerializer($companyUserShow);
            $data = $companyUserShowSerializer->serialize();

            return $this->success($data);
        } else {
            return $this->error(Lang::get('response.access_forbiden'));
        }
    }

    public function removeCompanyUser($data, $userId, $roleId)
    {

        if ($this->authorizationManagerService->CompanyCanAccessUser($userId, [$data['company_id']], $roleId)) {
            $this->companyUserRepository->removeCompanyUser($userId, $data['company_id']);
            $this->usersRepository->destroyUser($userId);
            return $this->success(null, Lang::get('response.success_insert_data'));
        } else {
            return $this->error(Lang::get('response.access_forbiden'));
        }
    }


}
