<?php

namespace App\Services;

use App\Constants\DealsConstants;
use App\Repositories\DealRepository;
use App\Repositories\DealStatusRepository;
use App\Serializers\Deals\DealIndexCollection;

class DealsService extends BaseService
{
    protected $repository;
    protected $dealStatusRepository;

    public function __construct(DealRepository $repository, DealStatusRepository $dealStatusRepository)
    {
        $this->repository = $repository;
        $this->dealStatusRepository = $dealStatusRepository;
    }

    public function getDealsByFilter($user, $filters)
    {
        //these are the only params that user can set
        $allowedFilterParams = ['id', 'name', 'sorting', 'page', 'status'];
        @$params = array_intersect_key($filters, array_flip($allowedFilterParams));

        $notAllowedStatusId = [
            DealsConstants::DealStatuses['REFUNDED']['id'],
            DealsConstants::DealStatuses['TRASH']['id']
        ];

        if(isset($filters['status']) && in_array($filters['status'], $notAllowedStatusId))
        {
            unset($params['status']);
        }

        list($count, $result) = $this->repository->getDealsByFilter($user, $params);
        //here we are serializing data to our customized one
        $dealIndexCollection = new DealIndexCollection($result);

        $status = $this->dealStatusRepository->getAllStatus();

        $data = [
            'count' => $count,
            'deals' => $dealIndexCollection,
            'status' => $status
        ];
        return $this->success($data);
    }
}
