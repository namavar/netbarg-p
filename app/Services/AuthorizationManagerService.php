<?php

namespace App\Services;


use App\Repositories\CompanyRepository;
use App\Repositories\CompanyUserRepository;
use App\Repositories\UserRepository;
use App\Constants\RoleConstants;

class AuthorizationManagerService extends BaseService
{
    protected $companyRepository;
    protected $companyUserRepository;
    protected $userRepository;

    public function __construct(CompanyRepository $companyRepository,UserRepository $userRepository, CompanyUserRepository $companyUserRepository)
    {
        $this->companyRepository = $companyRepository;
        $this->companyUserRepository = $companyUserRepository;
        $this->userRepository = $userRepository;
    }

    public function UserCanAccessCompany($companyId,$userId,$roleId)
    {
        if ( $roleId == RoleConstants::ROLE_ADMIN_ID) {
            return true;
        }

        $company = $this->companyRepository->getCompanyUserCount($companyId,$userId);
        if ($company == 0) {
            return false;
        }
        return true;
    }

    public function CompanyCanAccessUser($userId, $companies,$roleId)
    {
        if ($roleId == RoleConstants::ROLE_ADMIN_ID) {
            return true;
        }

        $company = $this->companyUserRepository->checkCompanyUser($userId ,$companies);
        if ($company == 0) {
            return false;
        }
        return true;
    }
}
