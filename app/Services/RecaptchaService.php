<?php

namespace App\Services;


class RecaptchaService extends BaseService
{
    private static $_siteVerifyUrl = "https://www.google.com/recaptcha/api/siteverify?";
    private $_secret = '6LfyZz0UAAAAAONT0ftuop-0poGwtk2z6DIv_IwL';
    public $success;
    public $errorCodes;

    public function verifyResponse($response, $remoteIp = '')
    {
        // Discard empty solution submissions
        if ($response == null || strlen($response) == 0) {
            $this->success = false;
            $this->errorCodes = 'missing-input';
            return $this;
        }

        $getResponse = $this->_submitHttpGet(
            self::$_siteVerifyUrl,
            array(
                'secret' => $this->_secret,
                'response' => $response
            )
        );

        $answers = json_decode($getResponse, true);
        if (trim($answers['success']) == true) {
            $this->success = true;
        } else {
            $this->success = false;
            $this->errorCodes = $answers ['error-codes'];
        }

        return $this;
    }

    private function _submitHTTPGet($path, $data)
    {
        $req = $this->_encodeQS($data);
        $response = file_get_contents($path . $req);
        return $response;
    }

    private function _encodeQS($data)
    {
        $req = "";
        foreach ($data as $key => $value) {
            $req .= $key . '=' . urlencode(stripslashes($value)) . '&';
        }
        // Cut the last '&'
        $req = substr($req, 0, strlen($req) - 1);
        return $req;
    }

}
