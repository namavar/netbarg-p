<?php

namespace App\Services;

use App\Repositories\UserProfileRepository;
use DB;
use Auth;

class UserProfileService extends BaseService
{
    /**
     * @var UserProfileRepository
     */
    protected $repository;
    protected $userProfileRepository;

    /**
     *
     * @param UserProfileRepository $userProfileRepository
     */
    public function __construct(UserProfileRepository $userProfileRepository)
    {
        $this->userProfileRepository = $userProfileRepository;
    }

}
