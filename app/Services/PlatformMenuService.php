<?php
namespace App\Services;

use App\Repositories\PlatformMenuRepository;
use App\Serializers\SideBarMenusSerializer;
use Illuminate\Support\Facades\Lang;

class PlatformMenuService extends BaseService
{
    protected $repository;

    public function __construct(PlatformMenuRepository $repository)
    {
        $this->repository = $repository;
    }

    public function createPlatformMenu($data)
    {
        try {
            $this->repository->createPlatformMenu($data);
            return $this->success('',Lang::get('response.platformMenuService.success_insert'));
        } catch (\Exception $e) {
            return $this->error(Lang::get('response.platformMenuService.error_insert_data'));
        }
    }

    public function getMenus($filters){
        $allowedFilterParams = ['page'];
        @$params = array_intersect_key($filters, array_flip($allowedFilterParams));
        return $this->repository->getMenus($params);
    }

    public function updatePlatformMenu($data,$id){
        if($this->repository->updatePlatformMenu($data,$id))
            return $this->success('',Lang::get('response.platformMenuService.success_update_data'));

        return $this->error(Lang::get('response.platformMenuService.error_update_data'));
    }

    public function getMenusWithUserRole($roleId){
        $result = $this->repository->getMenusWithUserRole($roleId);
        $menuList = new SideBarMenusSerializer($result);
        return $menuList->serialize();
    }

    public function getParents(){
        return $this->repository->getParents();
    }
}
