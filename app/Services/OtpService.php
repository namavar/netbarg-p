<?php

namespace App\Services;

use App\Repositories\SettingRepository;
use App\Repositories\UserProfileRepository;
use App\vendor\Otp\FourDigitsGenerator;
use App\vendor\Otp\OneHourSendLimit;
use App\vendor\Otp\OtpException;
use App\vendor\Otp\OtpMessageFormatter;
use App\vendor\Otp\OtpVerifier;
use App\vendor\Otp\RedisOtpStorage;
use App\vendor\Otp\SmsOtpSender;
use Illuminate\Support\Facades\Lang;

class OtpService extends BaseService
{
    protected $userProfileRepository;
    protected $settingRepository;

    public function __construct(UserProfileRepository $userProfileRepository, SettingRepository $settingRepository)
    {
        $this->userProfileRepository = $userProfileRepository;
        $this->settingRepository = $settingRepository;
    }

    public function send($phone, $userId = null)
    {
        if (!$this->isValidPhoneNumber($phone)) {
            return $this->error(Lang::get('response.otpService.invalid_phone_number'));
        }

        $phone = $this->normalizeMobileNumber($phone);

        if (!$userId && $this->isPhoneVerifiedBefore($phone,$userId)) {
            return $this->error(Lang::get('response.otpService.repeat_phone_number'));
        }

        try {
            $this->checkSendLimits($phone);
        } catch (OtpException $otpException) {
            return $this->error(Lang::get('response.otpService.access_denie_temperory'));
        }

        try {
            $this->sendOtp($phone);
        } catch (OtpException $otpException) {
            return $this->error(Lang::get('response.otpService.error_exist_retry'));
        }

        return $this->success(
            [
                'otpVerifyCaptcha' => $this->withoutCaptchaAttemptsExceeded($phone)
            ],
            Lang::get('response.otpService.send_four_number')
        );
    }

    public function verify($phone, $code, $captcha = null)
    {
        if ($this->withoutCaptchaAttemptsExceeded($phone)) {
            if (!isset($captcha) || empty($captcha)) {
                return $this->error(
                    Lang::get('respone.otpService.please_check_not_robot'),
                    [
                        'otpVerifyCaptcha' => true,
                        'reloadCaptcha' => true,
                    ]
                );
            }

            $recaptchaService = app()->make(RecaptchaService::class);
            $checkCaptcha = $recaptchaService->verifyResponse($captcha);
            if (!$checkCaptcha->success) {
                // captcha response is not correct
                return $this->error(
                    Lang::get('response.otpService.retry_please_check_not_robot'),
                    [
                        'otpVerifyCaptcha' => true,
                        'reloadCaptcha' => true,
                    ]
                );
            }
        }

        // convert arabic and persian numerals to english numerals
        $code = $this->convertNonEnglishNumerals($code);

        try {
            // verify OTP using verifier class
            $verifier = new OtpVerifier();
            $verify = $verifier->verify($code, $phone, new RedisOtpStorage());
            if (!$verify) {
                return $this->error(Lang::get('response.otpService.mistak_four_number'));
            }

            // delete data from hash map
            $this->removePhoneFromStorage($phone);

            return $this->success();

        } catch (OtpException $otpException) {
            /**
             * user has entered an expired code,
             * send a new one and inform user about it.
             */
            try {

                $this->sendOtp($phone);

            } catch (OtpException $exception) {
                // exception may occur while sending new OTP
                return $this->error(Lang::get('response.otpService.error_exist_send_code_retry'),
                    [
                        'otpVerifyCaptcha' => $this->withoutCaptchaAttemptsExceeded($phone),
                        'reloadCaptcha' => true,
                    ]);
            }

            return $this->error(Lang::get('response.otpService.expired_code_resend_new_code'));
        }
    }

    public function isValidPhoneNumber($phoneNumber)
    {
        return preg_match("#^(0|0098|\+98|98){0,1}9\d{9}$#", $phoneNumber);
    }

    public function normalizeMobileNumber($mobileNumber)
    {

        $mobileNumber = $this->convertNonEnglishNumerals($mobileNumber);

        $mobileNumber = preg_replace('#^0*98(9\d{9})$#', '0$1', $mobileNumber);
        $mobileNumber = preg_replace('#^(9\d{9})$#', '0$1', $mobileNumber);
        $mobileNumber = preg_replace('#^\+98(9\d{9})$#', '0$1', $mobileNumber);

        return $mobileNumber;
    }

    public function isPhoneVerifiedBefore($phoneNumber, $exceptUserId = null)
    {
        $phoneNumber = $this->normalizeMobileNumber($phoneNumber);

        $conditions = [
            'verified_phone' => 1,
            'phone' => $phoneNumber,
        ];

        if (!is_null($exceptUserId)) {
            $conditions['user_id'] = $exceptUserId;
        }

        $userProfile = $this->userProfileRepository->findWhere($conditions)->first();

        return !is_null($userProfile);
    }

    public function checkSendLimits($phone)
    {
        $settings = $this->getWebserviceCredentials();
        $sender = new SmsOtpSender($settings);
        return $sender->checkSendLimit($phone, new RedisOtpStorage(), new OneHourSendLimit());
    }

    private function getWebserviceCredentials()
    {
        $parentSettings = $this->settingRepository->findWhere(['slug' => 'sms'])->first();
        $settings = $this->settingRepository->findByField('parent_id', $parentSettings->id);

        $keyValueSetting = [];
        foreach ($settings as $key => $value) {
            $keyValueSetting[$value->slug] = $value->content;
        }

        return $keyValueSetting;
    }

    public function sendOtp($phone, $password = null)
    {
        $settings = $this->getWebserviceCredentials();
        $generator = new FourDigitsGenerator($password);
        $sender = new SmsOtpSender($settings);
        $store = new RedisOtpStorage();
        $sender->send($phone, $generator, new OtpMessageFormatter(), $store);
        $store->updateSentCount($phone, new OneHourSendLimit());
    }

    public function withoutCaptchaAttemptsExceeded($phone)
    {
//        dd($this->getFailedAttempts($phone));
        return $this->getFailedAttempts($phone) >= $this->getMinimumFailedAttemptsToShowCaptcha();
    }

    public function getFailedAttempts($phone)
    {
        $store = new RedisOtpStorage();

        return $store->getFailedAttemptsCount($phone);
    }

    public function getMinimumFailedAttemptsToShowCaptcha()
    {
        return 3;
    }

    public function removePhoneFromStorage($phone)
    {
        $store = new RedisOtpStorage();
        $store->remove($phone);

        return true;
    }

    public function convertNonEnglishNumerals($string)
    {
        $delimiter = '#';

        /**
         * It seems like most of arabic and persian numerals are equal (like 0 or 1),
         * but if you compare them using a simple if condition, you would
         * see they are not equal.
         *
         * (OR MAYBE JUST MY MISTAKE)
         */

        $nonEnglishNumerals = [
            // arabic
            $delimiter . '٠' . $delimiter,
            $delimiter . '١‬' . $delimiter,
            $delimiter . '٢' . $delimiter,
            $delimiter . '٣' . $delimiter,
            $delimiter . '٤‬' . $delimiter,
            $delimiter . '٥' . $delimiter,
            $delimiter . '٦' . $delimiter,
            $delimiter . '٧' . $delimiter,
            $delimiter . '٨' . $delimiter,
            $delimiter . '٩‬' . $delimiter,
            // persian
            $delimiter . '۰' . $delimiter,
            $delimiter . '۱' . $delimiter,
            $delimiter . '۲' . $delimiter,
            $delimiter . '۳' . $delimiter,
            $delimiter . '۴' . $delimiter,
            $delimiter . '۵' . $delimiter,
            $delimiter . '۶' . $delimiter,
            $delimiter . '۷' . $delimiter,
            $delimiter . '۸' . $delimiter,
            $delimiter . '۹' . $delimiter,
        ];
        $replacement = [
            // for arabic
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            // for persian
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        ];

        return preg_replace($nonEnglishNumerals, $replacement, $string);
    }
}
