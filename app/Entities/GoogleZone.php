<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GoogleZone.
 *
 * @package namespace App\Entities;
 */
class GoogleZone extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'latitude',
        'longitude'
    ];

}
