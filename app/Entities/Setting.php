<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Setting.
 *
 * @package namespace App\Entities;
 */
class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
