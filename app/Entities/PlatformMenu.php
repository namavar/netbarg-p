<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PlatformMenu.
 *
 * @package namespace App\Entities;
 */
class PlatformMenu extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'alias',
        'parent_id',
        'website',
        'controller',
        'action',
        'prefix',
        'plugin',
        'order_display',
        'active',
        'icon_class',
        'short_url',
        'lft',
        'rght',
        'trash',
    ];

    public function firstChildren()
    {
       return $this->hasMany('App\Entities\PlatformMenu', 'parent_id', 'id');
    }

    public function firstParent()
    {
       return $this->hasOne('App\Entities\PlatformMenu', 'id', 'parent_id');
    }

    public function children()
    {
       return $this->firstChildren()->with('children');
    }
}
