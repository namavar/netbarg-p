<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class Company.
 *
 * @package namespace App\Entities;
 */
class Company extends Model
{
    use Sluggable;

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'short_description',
        'website',
        'phone',
        'shaba_number',
        'sale_agent_phone',
        'is_discount',
        'has_sellercenter',
        'created_by',
        'active',
        'trash'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function address()
    {
        return $this->hasMany(CompanyAddress::class);
    }

    public function user()
    {
        return $this->hasMany(CompanyUser::class);
    }

}
