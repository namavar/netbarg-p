<?php

namespace App\Entities;

use App\Entities\Scopes\TrashScope;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TrashScope());
    }
}
