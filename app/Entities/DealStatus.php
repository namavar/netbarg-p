<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DealStatus extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
}
