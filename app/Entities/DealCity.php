<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DealCity extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    public function city()
    {
        return $this->belongsTo(City::class);
    }

}
