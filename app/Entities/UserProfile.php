<?php

namespace App\Entities;

use App\Entities\Scopes\TrashScope;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserProfile.
 *
 * @package namespace App\Entities;
 */
class UserProfile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','phone',
        'user_id','verified_phone', 'birthday',
        'gender_id', 'state_id', 'city_id', 'national_code'
    ];

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TrashScope());
    }

}
