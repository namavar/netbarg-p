<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AttachmentsRepository.
 *
 * @package namespace App\Entities;
 */
class Attachments extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identify',
        'filename',
        'class',
        'dir',
        'foreign_id',
        'priority',
        'active'
    ];


}
