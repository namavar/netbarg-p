<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CompanyAddress.
 *
 * @package namespace App\Entities;
 */
class CompanyAddress extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
        'phone',
        'address',
        'zip_code',
        'state_id',
        'city_id',
        'zone_id',
        'latitude',
        'longitude',
        'map_zoom_level',
        'company_id'
    ];

}
