<?php

namespace App\Entities;

use App\Entities\Scopes\TrashScope;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Role.
 *
 * @package namespace App\Entities;
 */
class Role extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','guard_name','trash'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TrashScope());
    }

    public function permission()
    {
        return $this->belongsToMany(Permission::class,'role_has_permissions');
    }
}
