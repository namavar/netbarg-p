<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CompanyUser.
 *
 * @package namespace App\Entities;
 */
class CompanyUser extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'user_id'
    ];

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function userProfile()
    {
        return $this->hasMany(UserProfile::class);
    }

}
