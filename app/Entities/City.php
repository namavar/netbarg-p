<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class City.
 *
 * @package namespace App\Entities;
 */
class City extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
