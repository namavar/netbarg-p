<?php

namespace App\Entities;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Log extends Eloquent {

    protected $connection = 'mongodb';
    protected $collection = 'logs_collection';
    protected $guarded= [];
}
