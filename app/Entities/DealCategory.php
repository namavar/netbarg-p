<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DealCategory extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
