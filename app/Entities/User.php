<?php

namespace App\Entities;

use App\Entities\Scopes\TrashScope;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User.
 *
 * @package namespace App\Entities;
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles;

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id','active',
    ];

    public $guard_name = 'api';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function UserProfile()
    {
        return $this->hasOne(UserProfile::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TrashScope());
    }

}
