<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Deal.
 *
 * @package namespace App\Entities;
 */
class Deal extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contract_id',
        'name',
        'short_name',
        'private_note',
        'desciption',
        'conditions',
        'highlights',
        'original_price',
        'discount_amount',
        'discount_percentage',
        'start_date',
        'end_date',
        'coupon_start_date',
        'coupon_end_date',
        'tax',
        'min_limit',
        'max_limit',
        'min_limit_per_user',
        'max_limit_per_user',
        'gender_id',
        'brand_id',
        'for_one_user',
        'hard_print_needed',
        'reserve_needed',
        'is_especial',
        'is_especial',
        'is_main',
        'is_postal',
        'category_id',
        'for_all_cities',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function status()
    {
        return $this->belongsTo(DealStatus::class, "deal_status_id");
    }

    public function dealCity()
    {
        return $this->hasMany(DealCity::class);
    }

    public function dealCategory()
    {
        return $this->hasMany(DealCategory::class);
    }
}
