<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Contracts.
 *
 * @package namespace App\Entities;
 */
class Contracts extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'company_name',
        'name',
        'national_code',
        'email',
        'user_id',
        'bank_name',
        'contract_owner_name',
        'sheba_number',
        'company_share'
    ];



}
