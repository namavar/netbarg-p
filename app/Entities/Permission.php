<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Permission.
 *
 * @package namespace App\Entities;
 */
class Permission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'uri', 'prefix', 'controller', 'guard_name'
    ];

    public function role()
    {
        return $this->belongsToMany(Role::class,'role_has_permissions');
    }
}
