<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ContractCategory.
 *
 * @package namespace App\Entities;
 */
class ContractCategory extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contract_id',
        'category_id',
    ];



}
