<?php

namespace App\Providers;

use App\Entities\Company;
use App\Entities\CompanyAddress;
use App\Entities\CompanyUser;
use App\Entities\ContractCategory;
use App\Entities\Contracts;
use App\Entities\Deal;
use App\Entities\Permission;
use App\Entities\PlatformMenu;
use App\Entities\Role;
use App\Entities\User;
use App\Entities\UserProfile;
use App\Observers\CompanyObserver;
use App\Observers\CompanyUserObserver;
use App\Observers\ContractCategoryObserver;
use App\Observers\ContractObserver;
use App\Observers\DealObserver;
use App\Observers\PermissionObserver;
use App\Observers\PlatformMenuObserver;
use App\Observers\RoleObserver;
use App\Observers\UserObserver;
use App\Observers\UserProfileObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        /*  Observer entity logs  */
        $this->Observers();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Observer List Entities
     */
    private function Observers()
    {
        Company::observe(CompanyObserver::class);
        CompanyAddress::observe(CompanyAddress::class);
        CompanyUser::observe(CompanyUserObserver::class);
        ContractCategory::observe(ContractCategoryObserver::class);
        Contracts::observe(ContractObserver::class);
        Deal::observe(DealObserver::class);
        Permission::observe(PermissionObserver::class);
        PlatformMenu::observe(PlatformMenuObserver::class);
        Role::observe(RoleObserver::class);
        User::observe(UserObserver::class);
        UserProfile::observe(UserProfileObserver::class);

    }

}
