<?php

namespace App\Observers;

use App\Entities\UserProfile;

class UserProfileObserver extends BaseObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Entities\UserProfile  $userProfile
     * @return void
     */
    public function created(UserProfile $userProfile)
    {
        $this->log($userProfile);
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Entities\UserProfile  $userProfile
     * @return void
     */
    public function updated(UserProfile $userProfile)
    {
        $this->log($userProfile);
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Entities\UserProfile  $userProfile
     * @return void
     */
    public function deleted(UserProfile $userProfile)
    {
        $this->log($userProfile);
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Entities\UserProfile  $userProfile
     * @return void
     */
    public function restored(UserProfile $userProfile)
    {

    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Entities\UserProfile  $userProfile
     * @return void
     */
    public function forceDeleted(UserProfile $userProfile)
    {
        //
    }
}
