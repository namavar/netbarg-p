<?php

namespace App\Observers;

use App\Entities\CompanyUser;

class CompanyUserObserver extends BaseObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Entities\CompanyUser  $companyUser
     * @return void
     */
    public function created(CompanyUser $companyUser)
    {
        $this->log($companyUser);
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Entities\CompanyUser  $companyUser
     * @return void
     */
    public function updated(CompanyUser $companyUser)
    {
        $this->log($companyUser);
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Entities\CompanyUser  $companyUser
     * @return void
     */
    public function deleted(CompanyUser $companyUser)
    {
        $this->log($companyUser);
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Entities\CompanyUser  $companyUser
     * @return void
     */
    public function restored(CompanyUser $companyUser)
    {

    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Entities\CompanyUser  $companyUser
     * @return void
     */
    public function forceDeleted(CompanyUser $companyUser)
    {
        //
    }
}
