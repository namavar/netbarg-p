<?php

namespace App\Observers;

use App\Entities\PlatformMenu;

class PlatformMenuObserver extends BaseObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Entities\PlatformMenu  $platformMenu
     * @return void
     */
    public function created(PlatformMenu $platformMenu)
    {
        $this->log($platformMenu);
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Entities\PlatformMenu  $platformMenu
     * @return void
     */
    public function updated(PlatformMenu $platformMenu)
    {
        $this->log($platformMenu);
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Entities\PlatformMenu  $platformMenu
     * @return void
     */
    public function deleted(PlatformMenu $platformMenu)
    {
        $this->log($platformMenu);
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Entities\PlatformMenu  $platformMenu
     * @return void
     */
    public function restored(PlatformMenu $platformMenu)
    {

    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Entities\PlatformMenu  $platformMenu
     * @return void
     */
    public function forceDeleted(PlatformMenu $platformMenu)
    {
        //
    }
}
