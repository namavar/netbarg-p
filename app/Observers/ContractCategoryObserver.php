<?php

namespace App\Observers;

use App\Entities\ContractCategory;

class ContractCategoryObserver extends BaseObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Entities\ContractCategory  $conractCategory
     * @return void
     */
    public function created(ContractCategory $conractCategory)
    {
        $this->log($conractCategory);
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Entities\CompanyUser  $conractCategory
     * @return void
     */
    public function updated(ContractCategory $conractCategory)
    {
        $this->log($conractCategory);
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Entities\ContractCategory  $conractCategory
     * @return void
     */
    public function deleted(ContractCategory $conractCategory)
    {
        $this->log($conractCategory);
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Entities\ContractCategory  $conractCategory
     * @return void
     */
    public function restored(ContractCategory $conractCategory)
    {

    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Entities\ContractCategory  $conractCategory
     * @return void
     */
    public function forceDeleted(ContractCategory $conractCategory)
    {
        //
    }
}
