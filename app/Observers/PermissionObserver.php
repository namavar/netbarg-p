<?php

namespace App\Observers;

use App\Entities\Permission;

class PermissionObserver extends BaseObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Entities\Permission  $permission
     * @return void
     */
    public function created(Permission $permission)
    {
        $this->log($permission);
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Entities\Permission  $permission
     * @return void
     */
    public function updated(Permission $permission)
    {
        $this->log($permission);
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Entities\Permission  $permission
     * @return void
     */
    public function deleted(Permission $permission)
    {
        $this->log($permission);
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Entities\Permission  $permission
     * @return void
     */
    public function restored(Permission $permission)
    {

    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Entities\Permission  $permission
     * @return void
     */
    public function forceDeleted(Permission $permission)
    {
        //
    }
}
