<?php

namespace App\Observers;

use App\Entities\CompanyAddress;

class CompanyAddressObserver extends BaseObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Entities\CompanyAddress  $companyAddress
     * @return void
     */
    public function created(CompanyAddress $companyAddress)
    {
        $this->log($companyAddress);
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Entities\CompanyAddress  $companyAddress
     * @return void
     */
    public function updated(CompanyAddress $companyAddress)
    {
        $this->log($companyAddress);
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Entities\CompanyAddress  $companyAddress
     * @return void
     */
    public function deleted(CompanyAddress $companyAddress)
    {
        $this->log($companyAddress);
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Entities\CompanyAddress  $companyAddress
     * @return void
     */
    public function restored(CompanyAddress $companyAddress)
    {

    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Entities\CompanyAddress  $companyAddress
     * @return void
     */
    public function forceDeleted(CompanyAddress $companyAddress)
    {
        //
    }
}
