<?php

namespace App\Observers;

use App\Entities\Company;

class CompanyObserver extends BaseObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Entities\Company  $company
     * @return void
     */
    public function created(Company $company)
    {
        $this->log($company);
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Entities\Company  $company
     * @return void
     */
    public function updated(Company $company)
    {
        $this->log($company);
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Entities\Company  $company
     * @return void
     */
    public function deleted(Company $company)
    {
        $this->log($company);
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Entities\Company  $company
     * @return void
     */
    public function restored(Company $company)
    {

    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Entities\Company  $company
     * @return void
     */
    public function forceDeleted(Company $company)
    {
        //
    }
}
