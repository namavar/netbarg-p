<?php

namespace App\Observers;

use App\Entities\Deal;

class DealObserver extends BaseObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Entities\Deal  $deal
     * @return void
     */
    public function created(Deal $deal)
    {
        $this->log($deal);
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Entities\CompanyUser  $deal
     * @return void
     */
    public function updated(Deal $deal)
    {
        $this->log($deal);
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Entities\Deal  $deal
     * @return void
     */
    public function deleted(Deal $deal)
    {
        $this->log($deal);
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Entities\Deal  $deal
     * @return void
     */
    public function restored(Deal $deal)
    {

    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Entities\Deal  $deal
     * @return void
     */
    public function forceDeleted(Deal $deal)
    {
        //
    }
}
