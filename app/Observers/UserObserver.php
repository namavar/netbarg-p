<?php

namespace App\Observers;

use App\Entities\User;

class UserObserver extends BaseObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Entities\User  $user
     * @return void
     */
    public function created(User $user)
    {
        $this->log($user);
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Entities\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        $this->log($user);
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Entities\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        $this->log($user);
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Entities\User  $user
     * @return void
     */
    public function restored(User $user)
    {

    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Entities\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
