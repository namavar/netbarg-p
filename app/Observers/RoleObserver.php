<?php

namespace App\Observers;

use App\Entities\Role;

class RoleObserver extends BaseObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Entities\Role  $role
     * @return void
     */
    public function created(Role $role)
    {
        $this->log($role);
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Entities\Role  $role
     * @return void
     */
    public function updated(Role $role)
    {
        $this->log($role);
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Entities\Role  $role
     * @return void
     */
    public function deleted(Role $role)
    {
        $this->log($role);
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Entities\Role  $role
     * @return void
     */
    public function restored(Role $role)
    {

    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Entities\Role  $role
     * @return void
     */
    public function forceDeleted(Role $role)
    {
        //
    }
}
