<?php

namespace App\Observers;

use App\Entities\Contracts;

class ContractObserver extends BaseObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param  \App\Entities\Contracts  $conract
     * @return void
     */
    public function created(Contracts $conract)
    {
        $this->log($conract);
    }

    /**
     * Handle the company "updated" event.
     *
     * @param  \App\Entities\CompanyUser  $conract
     * @return void
     */
    public function updated(Contracts $conract)
    {
        $this->log($conract);
    }

    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Entities\Contracts  $conract
     * @return void
     */
    public function deleted(Contracts $conract)
    {
        $this->log($conract);
    }

    /**
     * Handle the company "restored" event.
     *
     * @param  \App\Entities\Contracts  $conract
     * @return void
     */
    public function restored(Contracts $conract)
    {

    }

    /**
     * Handle the company "force deleted" event.
     *
     * @param  \App\Entities\Contracts  $conract
     * @return void
     */
    public function forceDeleted(Contracts $conract)
    {
        //
    }
}
