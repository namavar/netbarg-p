<?php

namespace App\Observers;

use App\Entities\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class BaseObserver
{
    public function log($model)
    {
        $url = Request::url();
        $dirtyField = $model->getDirty();
        $ip = Request::ip();
        $user = Auth::user();
        $userInfo = [
            'userId' => $user->id,
            'email' => $user->email,
            'firstName' => $user->UserProfile()->first()->first_name,
            'lastName' => $user->UserProfile()->first()->last_name,
            'ip' => $ip,
        ];
        $modelName = class_basename($model);
        Log::create([
            'userInfo' => $userInfo,
            'modelName' => $modelName,
            'dirtyField' => $dirtyField,
            'url' => $url
        ]);
    }
}
