FROM registry.imbco.ir/netbarg/platform-baseimage:v0.6

COPY . /var/www/html
WORKDIR /var/www/html
RUN chown -R nginx:nginx /var/www/html
RUN composer install
ENTRYPOINT ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord/conf.d/supervisord.conf"]
