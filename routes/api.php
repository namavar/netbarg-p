<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UsersController@login');
Route::post('otp-login', 'UsersController@otpLogin');
Route::post('register', 'UsersController@register');
Route::post('verify-phone-on-login', 'UsersController@verifyPhoneOnLogin');

Route::post('get-side-bar-menus', 'PlatformMenusController@getSideBarMenus')->middleware(['auth:api', 'acl']);

Route::post('logout', 'UsersController@logout')->middleware('auth:api');

Route::prefix('admin')->group(function () {
    Route::group(['middleware' => ['auth:api', 'acl']], function () {

        Route::prefix('permissions')->group(function () {
            Route::post('/', 'PermissionsController@index');
            Route::post('show', 'PermissionsController@show');
        });

        Route::prefix('role')->group(function () {
            Route::post('/', 'RolesController@index');
            Route::post('create', 'RolesController@store');
            Route::post('show/{id}', 'RolesController@show');
            Route::post('edit/{id}', 'RolesController@update');
            Route::post('delete/{id}', 'RolesController@destroy');
            Route::post('get-permitions', 'RolesController@getPermisions');
            Route::post('add-role-permission', 'RolesController@addRolePermission');
            Route::post('remove-role-permission', 'RolesController@removeRolePermission');
        });

        Route::prefix('menu')->group(function () {
            Route::post('/', 'PlatformMenusController@index');
            Route::post('create', 'PlatformMenusController@store');
            Route::post('update/{id}', 'PlatformMenusController@update');
            Route::post('get-parents', 'PlatformMenusController@getParents');
        });
    });
});

Route::prefix('company')->group(function () {
    Route::group(['middleware' => ['auth:api', 'acl']], function () {
        Route::post('/', 'CompaniesController@index');
        Route::post('create', 'CompaniesController@store');
        Route::post('show/{id}', 'CompaniesController@show');
        Route::post('edit/{id}', 'CompaniesController@update');
        Route::post('delete/{id}', 'CompaniesController@destroy');
        Route::get('get-cities', 'CompaniesController@getCities');
        Route::post('get-zones', 'CompaniesController@getZones');
        Route::post('user-companies', 'CompaniesController@userCompanies');
    });
});


Route::prefix('company-user')->group(function () {
    Route::group(['middleware' => ['auth:api', 'acl']], function () {

        Route::post('/', 'UserCompaniesController@index');
        Route::post('create', 'UserCompaniesController@store');
        Route::post('show/{id}', 'UserCompaniesController@show');
        Route::post('edit/{id}', 'UserCompaniesController@update');
        Route::post('delete/{id}', 'UserCompaniesController@destroy');
    });
});


Route::prefix('contract')->group(function () {
    Route::group(['middleware' => ['auth:api', 'acl']], function () {

        Route::post('/', 'ContractsController@index');
        Route::post('create', 'ContractsController@store');
        Route::post('show/{id}', 'ContractsController@show');
        Route::post('edit/{id}', 'ContractsController@update');
        Route::post('delete/{id}', 'ContractsController@destroy');

        Route::prefix('constants')->group(function (){
            Route::post('/categories','ContractsController@constantsCategories');
            Route::get('/role','ContractsController@getRole');
            Route::post('/get-required-category','ContractsController@getRequiredCategory');
        });

        Route::prefix('attachments')->group(function (){
            Route::post('','ContractsController@uploadFile');
            Route::post('/{id}','ContractsController@updateUploadFile');
        });
    });
});


Route::prefix('deals')->group(function (){
    Route::group(['middleware' => ['auth:api', 'acl']], function () {

        Route::post('/', 'DealsController@index');
        Route::post('create', 'DealsController@store');

    });
});

